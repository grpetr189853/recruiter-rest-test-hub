<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 27.03.2019
 * Time: 9:48
 */

namespace app\common\bootstrap;

use app\crm\dispatchers\SomeEventDispatcher;
use app\crm\interfaces\ICandidateRepository;
use app\crm\interfaces\ICountryRepository;
use app\crm\interfaces\IDocumentService;
use app\crm\interfaces\IUserRepository;
use app\crm\interfaces\IGroupRepository;
use app\crm\interfaces\ICityRepository;
use app\crm\interfaces\IContactService;
use app\crm\interfaces\ICustomerRepository;
use app\crm\interfaces\IEventDispatcher;
use app\crm\interfaces\ISkillRepository;
use app\crm\interfaces\ISkillService;
use app\crm\interfaces\IVacancyRepository;
use app\crm\repositories\candidates\ARCandidateRepository;
use app\crm\repositories\lib\CountryRepository;
use app\crm\repositories\users\ARUserRepository;
use app\crm\repositories\groups\ARGroupRepository;
use app\crm\repositories\customers\ARCustomerRepository;
use app\crm\repositories\lib\CityRepository;
use app\crm\repositories\lib\SkillRepository;
use app\crm\repositories\vacancies\ARVacancyRepository;
use app\crm\services\candidate\CandidateService;
use app\crm\services\CountryService;
use app\crm\services\DocumentService;
use app\crm\services\dto\CityCreateDTO;
use app\crm\services\dto\CountryCreateDTO;
use app\crm\services\dto\SkillCreateDTO;
use app\crm\services\user\UserService;
use app\crm\services\group\GroupService;
use app\crm\services\CityService;
use app\crm\services\ContactService;
use app\crm\services\customer\CustomerService;
use app\crm\services\dto\CustomerCreateDTO;
use app\crm\services\dto\VacancyCreateDTO;
use app\crm\services\vacancy\VacancyService;
use app\crm\services\dto\CandidateCreateDTO;
use app\crm\services\dto\UserCreateDTO;
use app\crm\services\dto\GroupCreateDTO;
use app\crm\services\SkillService;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use yii\base\BootstrapInterface;

class Setup implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;
        $container->setSingleton(ISkillService::class, SkillService::class);
        $container->setSingleton(ICandidateRepository::class, ARCandidateRepository::class);
        $container->setSingleton(ICustomerRepository::class, ARCustomerRepository::class);
        $container->setSingleton(IVacancyRepository::class, ARVacancyRepository::class);
        $container->setSingleton(IUserRepository::class, ARUserRepository::class);
        $container->setSingleton(IGroupRepository::class, ARGroupRepository::class);
        $container->setSingleton(IEventDispatcher::class, SomeEventDispatcher::class);
        $container->setSingleton(LazyLoadingValueHolderFactory::class);
        $container->setSingleton(IContactService::class, ContactService::class);
        $container->setSingleton(IDocumentService::class,DocumentService::class);
        //$container->setSingleton(CandidateService::class, function() use ($app) { return $app->candidateService; });
        $container->setSingleton('CandidateService',CandidateService::class);
        $container->setSingleton('CandidateRepository',ARCandidateRepository::class);
        $container->setSingleton('CustomerService',CustomerService::class);
        $container->setSingleton('CustomerRepository',ARCustomerRepository::class);
        $container->setSingleton('VacancyService', VacancyService::class);
        $container->setSingleton('VacancyRepository',ARVacancyRepository::class);
        $container->setSingleton('UserService', UserService::class);
        $container->setSingleton('UserRepository',ARUserRepository::class);
        $container->setSingleton('GroupService', GroupService::class);
        $container->setSingleton('GroupRepository',ARGroupRepository::class);

        $container->setSingleton(ICityRepository::class, CityRepository::class);
        $container->setSingleton('CityService',CityService::class);
        $container->setSingleton('SkillService', SkillService::class);
        $container->setSingleton(ICountryRepository::class, CountryRepository::class);
        $container->setSingleton('CountryService', CountryService::class);
        $container->setSingleton(ISkillRepository::class,SkillRepository::class);
        $container->setSingleton('SkillService',SkillService::class);
        $container->setSingleton('DocumentService',DocumentService::class);
        $container->setSingleton('candidateDTO',CandidateCreateDTO::class);
        $container->setSingleton('customerDTO',CustomerCreateDTO::class);
        $container->setSingleton('vacancyDTO',VacancyCreateDTO::class);
        $container->setSingleton('userDTO',UserCreateDTO::class);
        $container->setSingleton('groupDTO',GroupCreateDTO::class);
        $container->setSingleton('cityDTO',CityCreateDTO::class);
        $container->setSingleton('skillDTO', SkillCreateDTO::class);
        $container->setSingleton('countryDTO', CountryCreateDTO::class);

    }
}