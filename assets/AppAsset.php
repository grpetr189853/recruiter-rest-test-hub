<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'angular/dist/runtime-es2015.js',
        'angular/dist/polyfills-es2015.js',
        'angular/dist/vendor-es2015.js',
        'angular/dist/main-es2015.js',
        'angular/dist/styles-es2015.js',
        'angular/dist/assets/pdf.js',
        'angular/dist/assets/pdf-es5.js',
        'angular/dist/assets/pdf.worker-es5.js',
        'angular/dist/assets/viewer-es5.js',
        'angular/dist/assets/viewer.js',
        'angular/dist/assets/pdf.worker.js'
    ];
    public $depends = [
        'app\assets\FAAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
