import { TestBed } from '@angular/core/testing';

import { VacancyDocumentsService } from './vacancy-documents.service';

describe('VacancyDocumentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VacancyDocumentsService = TestBed.get(VacancyDocumentsService);
    expect(service).toBeTruthy();
  });
});
