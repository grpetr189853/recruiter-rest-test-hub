import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Company } from '../models/company';
@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private companyUrl = 'web/companies';  // URL to web api
  private expandRelatedStatuses = '?expand=relatedStatuses';
  constructor(private http: Http) { }

  getDetail(id: string): Promise<Company> {
    return this.http.get(`${this.companyUrl}/${id}`)
      .toPromise()
      .then(response => response.json() as Company)
      .catch(this.handleError);
  }

  getAllCompanies(): Promise<[Company]> {
    return this.http.get(`${this.companyUrl}`)
      .toPromise()
      .then(response => response.json() as Company[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
