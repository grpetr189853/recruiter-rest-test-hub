import { Injectable } from '@angular/core';
import { ContactsCandidate } from "../models/contactsCandidate";
import {Headers, Http} from "@angular/http";
import {switchMap} from "rxjs/operators";
import {Params} from "@angular/router";
import {City} from "../models/city";

@Injectable({
  providedIn: 'root'
})
export class ContactsCandidateService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private candidateUrl = `web/candidates/`;
  private contactsCandidateUrl = `/contacts-candidate`;
  constructor(private http: Http) { }
  getCandidateContacts(candidate_id: string): Promise<ContactsCandidate[]> {
    return this.http
      .get(this.candidateUrl + `${candidate_id}` + this.contactsCandidateUrl)
      .toPromise()
      .then(response => response.json() as ContactsCandidate)
      .catch(this.handleError);
  }
  getCandidateContactTypeName(contact_id: string) {
    const contactsCandidateTypeUrl = 'web/contacts-type/';
    return this.http
      .get(`${contactsCandidateTypeUrl}${contact_id}`)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  async addContactCandidate(contactCandidate: ContactsCandidate) {

    const candidateId = contactCandidate.candidate_id;

    return await this.http
      .post(this.candidateUrl + candidateId + this.contactsCandidateUrl, JSON.stringify({
        candidate_id: candidateId,
        contact_type: contactCandidate.contact_type,
        value: contactCandidate.value,
      }), {headers: this.headers})
      .toPromise()
      .then(res => {console.log('addedContactValue', contactCandidate.value); return res.json() as ContactsCandidate;})
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
