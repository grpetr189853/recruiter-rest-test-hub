import { TestBed } from '@angular/core/testing';

import { ContactsCustomerService } from './contacts-customer.service';

describe('ContactsCustomerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactsCustomerService = TestBed.get(ContactsCustomerService);
    expect(service).toBeTruthy();
  });
});
