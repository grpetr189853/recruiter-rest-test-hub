import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import {Candidate} from "../models/candidate";
import {City} from "../models/city";

@Injectable({
  providedIn: 'root'
})
export class CitiesService {


  private headers = new Headers({'Content-Type': 'application/json'});
  private citiesUrl = 'web/cities';  // URL to web api

  constructor(private http: Http) { }

  getData(): Promise<City[]> {
    return this.http.get(this.citiesUrl)
      .toPromise()
      .then(response => response.json() as City[])
      .catch(this.handleError);
  }

  getDetail(id: string): Promise<City> {
    return this.http.get(`${this.citiesUrl}/${id}`)
      .toPromise()
      .then(response => response.json() as City)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
  update(city: City): Promise<City> {
    const url = `${this.citiesUrl}/${city.id}`;
    return this.http
      .put(url, JSON.stringify(city), {headers: this.headers})
      .toPromise()
      .then(() => city)
      .catch(this.handleError);
  }
}
