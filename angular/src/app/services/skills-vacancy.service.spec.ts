import { TestBed } from '@angular/core/testing';

import { SkillsVacancyService } from './skills-vacancy.service';

describe('SkillsVacancyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SkillsVacancyService = TestBed.get(SkillsVacancyService);
    expect(service).toBeTruthy();
  });
});
