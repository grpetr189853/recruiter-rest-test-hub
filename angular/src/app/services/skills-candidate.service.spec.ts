import { TestBed } from '@angular/core/testing';

import { SkillsCandidateService } from './skills-candidate.service';

describe('SkillsCandidateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SkillsCandidateService = TestBed.get(SkillsCandidateService);
    expect(service).toBeTruthy();
  });
});
