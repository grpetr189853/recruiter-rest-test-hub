import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Vacancy } from '../models/vacancy';
import { HttpClient,HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class VacancyService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private vacancyUrl = 'web/vacancies';  // URL to web api

  constructor(private http: Http) { }

  getData(): Promise<Vacancy[]> {
    return this.http.get(this.vacancyUrl)
      .toPromise()
      .then(response => response.json() as Vacancy[])
      .catch(this.handleError);
  }

  
  update(vacancy: Vacancy): Promise<Vacancy> {
    const url = `${this.vacancyUrl}/${vacancy.id}`;
    return this.http
      .put(url, JSON.stringify(vacancy), {headers: this.headers})
      .toPromise()
      .then(() => vacancy)
      .catch(this.handleError);
  }

  create(vacancy: Vacancy): Promise<Vacancy> {
    const url = `${this.vacancyUrl}`;
    return this.http
      .post(url, JSON.stringify({
        name: vacancy.name,
        company_id: vacancy.company_id,
        location: vacancy.location,
        salary: vacancy.salary,
        //skills here
        v_duties: vacancy.v_duties,
        w_conditions: vacancy.w_conditions,
        v_requirements: vacancy.v_requirements,
        v_description: vacancy.v_description,
      }),{headers: this.headers})
      .toPromise()
      .then(res => res.json() as Vacancy)
      .catch(this.handleError);
  }
  
  delete(vacancy_id: string) {
    const url = `${this.vacancyUrl}/${vacancy_id}`;
    return this.http
      .delete(url,{headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
