import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import {Skill} from "../models/skill";
import {SkillsCandidate} from "../models/skillsCandidate";
import {switchMap} from "rxjs/operators";
import {Params} from "@angular/router";
@Injectable({
  providedIn: 'root'
})
export class SkillsCandidateService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private candidateUrl = `web/candidates/`;
  private skillsCandidateUrl = `/skills-candidate`;
  constructor(private http: Http) { }
  getAllCandidatesSkills(candidate_id) {
    return this.http.get(this.candidateUrl + `${candidate_id}` + '?expand=relatedSkills&fields=relatedSkills')
      .toPromise()
      .then(response => response.json() as SkillsCandidate[])
      .catch(this.handleError);
  }
  getData(candidate_id): Promise<SkillsCandidate[]> {
    console.log(this.candidateUrl + candidate_id + this.skillsCandidateUrl);
    return this.http.get(this.candidateUrl + candidate_id + this.skillsCandidateUrl)
      .toPromise()
      .then(response => response.json() as SkillsCandidate[])
      .catch(this.handleError);
  }
  async addSkillCandidate(skillCandidate: SkillsCandidate) {
    const skillId = skillCandidate.skill_id;
    const candidateId = skillCandidate.candidate_id;

    return await this.http
      .post(this.candidateUrl + candidateId + this.skillsCandidateUrl, JSON.stringify({
          skill_id: skillId,
          candidate_id: candidateId,
        }), {headers: this.headers})
          .toPromise()
          .then(res => {console.log('addedSkillId', skillId); return res.json() as SkillsCandidate;})
          .catch(this.handleError);
  }
  async removeSkillCandidate(id, candidate_id) {
    const deletedId = `${id}`;
    return await this.http
      .delete(this.candidateUrl + candidate_id + this.skillsCandidateUrl + '/' + deletedId)
      .toPromise()
      .then( () => console.log("DeletedSkillId", deletedId)).catch(this.handleError);
  }
  async removeAllSkillsCandidate(candidate_id) {
    return await this.getData(candidate_id)
      .then(skillCandidate => skillCandidate.forEach(skill => this.http
        .delete(this.candidateUrl + candidate_id + this.skillsCandidateUrl + '/' + skill.id)
        .toPromise()
        .then(res => { console.log('deletedSkillId', skill.id, 'deleted Skill_id', skill.skill_id); return res.json()})
        .catch(this.handleError)));
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
