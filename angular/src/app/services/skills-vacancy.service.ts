import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import {Skill} from "../models/skill";
import {SkillsVacancy} from "../models/skillsVacancy";
import {switchMap} from "rxjs/operators";
import {Params} from "@angular/router";
@Injectable({
  providedIn: 'root'
})
export class SkillsVacancyService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private vacancyUrl = `web/vacancies/`;
  private skillsVacancyUrl = `/skills-vacancy`;
  constructor(private http: Http) { }
  getAllVacanciesSkills(vacancy_id) {
    return this.http.get(this.vacancyUrl + `${vacancy_id}` + '?expand=relatedSkills&fields=relatedSkills')
      .toPromise()
      .then(response => response.json() as SkillsVacancy[])
      .catch(this.handleError);
  }
  getData(vacancy_id): Promise<SkillsVacancy[]> {
    console.log(this.vacancyUrl + vacancy_id + this.skillsVacancyUrl);
    return this.http.get(this.vacancyUrl + vacancy_id + this.skillsVacancyUrl)
      .toPromise()
      .then(response => response.json() as SkillsVacancy[])
      .catch(this.handleError);
  }
  async addSkillVacancy(skillVacancy: SkillsVacancy) {
    const skillId = skillVacancy.skill_id;
    const vacancyId = skillVacancy.vacancy_id;

    return await this.http
      .post(this.vacancyUrl + vacancyId + this.skillsVacancyUrl, JSON.stringify({
          skill_id: skillId,
          vacancy_id: vacancyId,
        }), {headers: this.headers})
          .toPromise()
          .then(res => {console.log('addedSkillId', skillId); return res.json() as SkillsVacancy;})
          .catch(this.handleError);
  }
  async removeSkillVacancy(id, vacancy_id) {
    const deletedId = `${id}`;
    return await this.http
      .delete(this.vacancyUrl + vacancy_id + this.skillsVacancyUrl + '/' + deletedId)
      .toPromise()
      .then( () => console.log("DeletedSkillId", deletedId)).catch(this.handleError);
  }
  async removeAllSkillsVacancy(vacancy_id) {
    return await this.getData(vacancy_id)
      .then(skillVacancy => skillVacancy.forEach(skill => this.http
        .delete(this.vacancyUrl + vacancy_id + this.skillsVacancyUrl + '/' + skill.id)
        .toPromise()
        .then(res => { console.log('deletedSkillId', skill.id, 'deleted Skill_id', skill.skill_id); return res.json()})
        .catch(this.handleError)));
  }    
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
