import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import {Skill} from "../models/skill";

@Injectable({
  providedIn: 'root'
})
export class SkillsService {


  private headers = new Headers({'Content-Type': 'application/json'});
  private skillsUrl = 'web/skills';  // URL to web api

  constructor(private http: Http) { }

  getData(): Promise<Skill[]> {
    return this.http.get(this.skillsUrl)
      .toPromise()
      .then(response => response.json() as Skill[])
      .catch(this.handleError);
  }

  getDetail(id: string): Promise<Skill> {
    return this.http.get(`${this.skillsUrl}/${id}`)
      .toPromise()
      .then(response => response.json() as Skill)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
  update(skill: Skill): Promise<Skill> {
    const url = `${this.skillsUrl}/${skill.id}`;
    return this.http
      .put(url, JSON.stringify(skill), {headers: this.headers})
      .toPromise()
      .then(() => skill)
      .catch(this.handleError);
  }
}
