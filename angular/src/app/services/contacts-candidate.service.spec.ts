import { TestBed } from '@angular/core/testing';

import { ContactsCandidateService } from './contacts-candidate.service';

describe('ContactsCandidateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactsCandidateService = TestBed.get(ContactsCandidateService);
    expect(service).toBeTruthy();
  });
});
