import { Pipe, PipeTransform, Inject, forwardRef } from '@angular/core';
import { CandidateListComponent } from '../components/candidate-list/candidate-list.component';
import { CandidateService } from '../services/candidate.service';

@Pipe({
  name: 'searchPipe'
})
export class SearchPipePipe implements PipeTransform {
  /*****Candidate Statuses*****/
  readonly STATUS_ALL = 0;
  readonly STATUS_NEW = 1;
  readonly STATUS_CONSIDERED = 2;
  readonly STATUS_TODAY = 3;
  readonly STATUS_OFFER = 4;
  readonly STATUS_SELECTED = 5;
  readonly STATUS_BLACKLISTED = 6;
  constructor(@Inject(forwardRef(() => CandidateListComponent)) private candidateListComponent:CandidateListComponent) {

  }
  transform(value: any, ...args: any[]): any {

    if (!args[0]) {
      return value;
    }

    if(value && value.length){
      return value.filter(el => {
        return (el.lastname.toLowerCase().indexOf(args[0].toLocaleLowerCase()) > -1)
        ||(el.name.toLowerCase().indexOf(args[0].toLocaleLowerCase()) > -1)
        ||(el.fathername.toLowerCase().indexOf(args[0].toLocaleLowerCase()) > -1)
      });
    } else {

      if(args[1] === undefined||args[1] === this.STATUS_ALL) {
        this.candidateListComponent.getCandidates();
      } else if(args[1]!==undefined||args[1] !== this.STATUS_ALL) {
        value = undefined;
        this.candidateListComponent.getCandidatesWithStatuses(args[1]);
      }
    }


  }

}
