import { Pipe, PipeTransform, Inject, forwardRef } from '@angular/core';
import { CandidateListComponent } from '../components/candidate-list/candidate-list.component';
import { EventBusService } from '../services/shared/event-bus.service';
import { EventData } from 'src/app/services/shared/event.class';
@Pipe({
  name: 'selectedCandidate'
})
export class SelectedCandidatePipe implements PipeTransform {

  constructor(
    @Inject(forwardRef(() => CandidateListComponent)) private candidateListComponent:CandidateListComponent,
    private eventBusService: EventBusService,
  ) {

  }
  
  transform(value: any, ...args: any[]): any {
    if(value && value.length) {
      this.candidateListComponent.selectedCandidate = value[0];
      this.eventBusService.emit(new EventData('selectedCandidate', value[0]));    
    }
    return value;
  }

}
