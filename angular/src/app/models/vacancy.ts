export class Vacancy {
    id?: string;
    name: string;
    created_at?: string;
    updated_at?: string;
    location?: string;
    city_name?:string;
    company_name?: string;
    company_id?: string;
    customer_id?: string;
    salary: number;
    v_duties?: string;
    w_conditions?: string;
    v_requirements?: string;
    v_description?: string;
}