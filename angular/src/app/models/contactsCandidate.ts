export class ContactsCandidate
{
  id?: number;
  candidate_id: string;
  contact_type: string;
  value: string;
}
