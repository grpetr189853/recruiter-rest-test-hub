import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS, MatDateFormats } from "@angular/material";
import * as moment from 'moment';
import {timestamp} from "rxjs/operators";

export class AppDateAdapter extends NativeDateAdapter {

  parse(value: any): Date | null {

    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');
      const year = Number(str[2]);
      const month = Number(str[1]) - 1;
      const date = Number(str[0]);
      return new Date(year, month, date);
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }
  format(date: Date, displayFormat: string): string {

    if (displayFormat == "input") {
      let day = date.getDate();
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      console.log(this.convert(year + '-' + this._to2digit(month) + '-' + this._to2digit(day)));
      return   this.convert(year + '-' + this._to2digit(month) + '-' + this._to2digit(day))   ;
    } else if (displayFormat == "inputMonth") {
      let month = date.getMonth() + 1;
      let year = date.getFullYear();
      console.log(this.convert(year + '-' + this._to2digit(month)));
      return  this.convert(year + '-' + this._to2digit(month));
    } else {
      console.log(this.convert(date.toDateString()));
      return this.convert(date.toDateString());
    }
    // const isoDateString: string = date.toISOString();
    // const isoDate = new Date(isoDateString);
    // const mySQLDateString = isoDate.toJSON().slice(0, 19).replace('T', ' ');
    // return mySQLDateString;

  }
  private convert(str) {
    let date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }
}

export const APP_DATE_FORMATS =
  {
    parse: {
      dateInput: {month: 'short', year: 'numeric', day: 'numeric'}
    },
    display: {
      dateInput: 'input',
      monthYearLabel: 'inputMonth',
      dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
      monthYearA11yLabel: {year: 'numeric', month: 'long'},
    }
  }
