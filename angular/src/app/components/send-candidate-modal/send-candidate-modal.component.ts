import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ContactsCandidateService } from 'src/app/services/contacts-candidate.service';
import { FileService } from 'src/app/services/file.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead/typeahead-match.class';
import { NgForm } from '@angular/forms';
import { MailService } from 'src/app/services/mail.service';

class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-send-candidate-modal',
  templateUrl: './send-candidate-modal.component.html',
  styleUrls: ['./send-candidate-modal.component.css']
})
export class SendCandidateModalComponent implements OnInit {
  title: string;
  closeBtnName: string;

  candidate_contacts_values: any = [];
  transformed_candidate_contacts_names: any = [];
  selected_contact_name: any;
  choosen_candidate_contacts_values : any = [];
  selected_contact_value: any;
  attached_documents: any;
  selected_attached_document: any;
  candidate_letter_body: any;
  selectedCandidateId: string;
  selectedCandidate: any;
  selectedAttachedFile: any;
  @ViewChild('fileInputDocument', {static: false}) fileInputDocument:ElementRef;
  readonly IS_DOCUMENT = 0;
  constructor(
    public bsModalRef: BsModalRef,
    private contactCandidateService: ContactsCandidateService,
    private fileService: FileService,
    private mailService: MailService,
  ) { }

  ngOnInit() {
    this.getCandidateContacts();
    this.getCandidateContactsNames();
    this.getAttachedDocuments();
  }

  getCandidateContacts() {
    this.contactCandidateService.getCandidateContacts(this.selectedCandidateId).then(
      contactsValues => contactsValues.forEach(contactValue => this.candidate_contacts_values.push(contactValue))
    )
  }

  getCandidateContactsNames() {
    this.contactCandidateService.getCandidateContacts(this.selectedCandidateId).then(
      contacts => contacts.forEach(contact => this.contactCandidateService.getCandidateContactTypeName(contact.contact_type).then( contactName => this.transformed_candidate_contacts_names.push(contactName)))
    )
  }

  onSelectCommunication(event: TypeaheadMatch): void{
    console.log(this.candidate_contacts_values);
    this.choosen_candidate_contacts_values = this.candidate_contacts_values.filter(candidateContactValue => candidateContactValue.contact_type == event.item.id);
  }

  getAttachedDocuments() {
    this.fileService.getCandidateDocuments(this.selectedCandidateId).then(
      candidateDocuments => {
        this.attached_documents = candidateDocuments.map(candidateDocument => candidateDocument.file_name);
      }
    )
  }

  addDocument(fileInput: any) {
    const file: File = fileInput.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {
      this.fileInputDocument.nativeElement.value = null;
      this.selectedAttachedFile = new FileSnippet(event.target.result, file);
      this.fileService.uploadFile(this.selectedAttachedFile.file, this.selectedCandidateId, this.IS_DOCUMENT).subscribe(
        (res) => {
          this.getAttachedDocuments();
          console.log(res);
        },
        (err) => {
          console.log(err);
        });      
    });

    reader.readAsDataURL(file);
  }

  onFormSubmit(sendCustomer: NgForm) {
    this.mailService.sendMail(
      undefined,
      undefined,
      this.selected_contact_name,
      this.selected_contact_value,
      this.selected_attached_document,
      this.candidate_letter_body
      );
    this.bsModalRef.hide();
  }

}
