import {Component, OnInit, OnDestroy, ViewChild, TemplateRef, ElementRef} from '@angular/core';

import { CandidateService} from '../../services/candidate.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Candidate} from '../../models/candidate';
import { switchMap, map } from 'rxjs/operators';
import {City} from "../../models/city";
import {CitiesService} from "../../services/cities.service";
import {SkillsService} from "../../services/skills.service";
import {Skill} from "../../models/skill";
import {SkillsCandidateService} from "../../services/skills-candidate.service";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {CKEditorComponent} from "@ckeditor/ckeditor5-angular";
import {NgForm} from "@angular/forms";
import {SkillsCandidate} from "../../models/skillsCandidate";
import {ImageService} from "../../services/image.service";
import {FileService} from "../../services/file.service";
import {DocumentCandidate} from "../../models/DocumentCandidate";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from "@angular/router";
class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}

class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-candidate-update',
  templateUrl: './candidate-update.component.html',
  styleUrls: ['./candidate-update.component.css']
})
export class CandidateUpdateComponent implements OnInit {
  @ViewChild('fileInputResume', {static: false}) fileInputResume:ElementRef;
  @ViewChild('fileInputDocument', {static: false}) fileInputDocument:ElementRef;
  @ViewChild('photoInput', {static: false}) photoInput:ElementRef;
  readonly IS_RESUME = 1;
  readonly IS_DOCUMENT = 0;
  candidate: Candidate;
  cities: City[];
  skills: Skill[];
  selectedSkills: Array<any> = Array();
  skillsCandidate: SkillsCandidate[];
  public Editor = ClassicEditor;
  isSelectedSkill: boolean;
  selectedPhoto: ImageSnippet;
  selectedFile: FileSnippet;
  selectedFileDocument: FileSnippet;
  isPhotoDeleted: boolean = false;
  candidateResume: any = undefined;
  candidateDocuments: any;
  deletedCandidateDocument: any;
  modalRef: BsModalRef;
  message: string;
  constructor(
    private candidateService: CandidateService,
    private route: ActivatedRoute,
    private citiesService: CitiesService,
    private skillsService: SkillsService,
    private skillsCandidateService: SkillsCandidateService,
    private imageService: ImageService,
    private fileService: FileService,
    private modalService: BsModalService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getCandidates();
    this.getCities();
    this.getSkills();
    this.getCandidateResume();
    this.getCandidateDocuments();
  }


  getCities() {
    this.route.params.pipe(switchMap((params: Params) => this.citiesService.getData()))
      .subscribe( cities => this.cities = cities);
  }

  getSkills() {
    this.route.params.pipe(switchMap((params: Params) => this.skillsService.getData()))
      .subscribe( skills => {
        this.skills = skills;
      });
  }
  getCandidates() {
    this.route.params.pipe(switchMap((params: Params) => this.candidateService.getDetail(params['id'])))
      .subscribe( candidate => {
        this.candidate = candidate;
        this.getSkillsCandidates(this.candidate.id);

      });
  }
  getSkillsCandidates(id) {
    this.route.params.pipe(switchMap((params: Params) => this.skillsCandidateService.getData(id)))
      .subscribe( skillsCandidate => {
        this.skillsCandidate = skillsCandidate;
        this.isSelectedSkill = true;
        const candSkillIds = this.skillsCandidate.map(i => i.skill_id);
        this.skills.forEach(item => {
          if (candSkillIds.includes(item.id)) {
            this.selectedSkills.push(item);
          }
        });
      });
    console.log(this.selectedSkills);
    console.dir(this.selectedSkills);
  }
  getCandidateResume() {
    this.route.params.pipe(switchMap((params: Params) => this.fileService.getCandidateResume(params['id'])))
      .subscribe(documentCandidate => {
          if (Object.keys(documentCandidate).length !== 0) {
            this.candidateResume = documentCandidate.filter(doc => doc.is_resume === this.IS_RESUME );
          } else {
            this.candidateResume = undefined;
          }
        });
  }
  getCandidateDocuments() {
    this.route.params.pipe(switchMap((params: Params) => this.fileService.getCandidateDocuments(params['id'])))
      .subscribe(documentCandidate =>  {
        this.candidateDocuments = documentCandidate.filter(doc => doc.is_resume === this.IS_DOCUMENT );
      });
  }
  comparer(o1: Skill, o2: Skill): boolean {
    // if possible compare by object's name property - and not by reference.
    return o1 && o2 ? o1.name === o2.name : o2 === o2;
  }
  onFormSubmit(candidateUpdateForm: NgForm) {
    // this.skillsCandidateService.removeAllSkillsCandidate(this.candidate.id)
      // .then((value: any) => this.selectedSkills.forEach(skill => this.skillsCandidateService.addSkillCandidate({skill_id: skill.id, candidate_id: this.candidate.id})));
      (async () => {
        await this.skillsCandidateService.removeAllSkillsCandidate(this.candidate.id);
        await this.selectedSkills.forEach(skill => this.skillsCandidateService.addSkillCandidate({skill_id: skill.id, candidate_id: this.candidate.id}));    
      })();
    this.candidateService.update(this.candidate);
  }

  private onSuccess() {
    this.selectedPhoto.pending = false;
    this.selectedPhoto.status = 'ok';
  }

  private onError() {
    this.selectedPhoto.pending = false;
    this.selectedPhoto.status = 'fail';
    this.selectedPhoto.src = '';
  }

  private onFileSuccess() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'ok';
  }

  private onFileError() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'fail';
    this.selectedFile.src = '';
  }

  processPhoto(photoInput: any) {
    const file: File = photoInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.photoInput.nativeElement.value = null;
      this.selectedPhoto = new ImageSnippet(event.target.result, file);
      this.selectedPhoto.pending = true;
      this.imageService.uploadPhoto(this.selectedPhoto.file, this.candidate.id).subscribe(
        (res) => {
          this.isPhotoDeleted = false;
          console.log(res);
          this.onSuccess();
        },
        (err) => {
          this.onError();
        });
    });

    reader.readAsDataURL(file);
  }

  removePhoto() {
    this.imageService.deletePhoto(this.candidate.id).subscribe(
      (res) => {
        this.isPhotoDeleted = true;
        this.selectedPhoto = undefined;
        this.candidate.photo = undefined;
        console.log(res);
      },
      (err) => {
        console.log(err);
      });
  }

  processFile(fileInput: any, fileType: number) {
    const file: File = fileInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.fileInputResume.nativeElement.value = null;
      this.fileInputDocument.nativeElement.value = null;
      if(fileType === this.IS_RESUME) {
        this.selectedFile = new FileSnippet(event.target.result, file);
        this.fileService.uploadFile(this.selectedFile.file, this.candidate.id, fileType).subscribe(
          (res) => {
            this.onFileSuccess();
          },
          (err) => {
            this.onFileError();
          });
      } else if (fileType === this.IS_DOCUMENT) {
        this.selectedFileDocument = new FileSnippet(event.target.result, file);
        this.fileService.uploadFile(this.selectedFileDocument.file, this.candidate.id, fileType).subscribe(
          (res) => {
            this.getCandidateDocuments();
          },
          (err) => {

          });
      }

    });

    reader.readAsDataURL(file);
  }

  removeFile($event) {
    $event.preventDefault();
    this.route.params.pipe(switchMap((params: Params) => this.fileService.getCandidateDocuments(params['id']) ))
      .subscribe(documentCandidate => {
        const candidateResume = documentCandidate.filter( doc => doc.is_resume = this.IS_RESUME).pop();
        this.fileService.deleteFileByName(this.candidate.id, candidateResume.id).subscribe(
          (res) => {
            this.selectedFile = undefined;
            this.candidateResume = undefined;
            this.getCandidateDocuments();
            console.log(res);
          },
          (err) => {
            console.log(err);
          });    
      })
  }

  removeCandidateDocumentByName($event, fileName: string) {
    $event.preventDefault();
    this.route.params.pipe(switchMap((params: Params) => this.fileService.getCandidateDocuments(params['id'])))
      .subscribe(documentCandidate =>  {
        this.deletedCandidateDocument = documentCandidate.filter(doc => doc.file_name === fileName ).pop();
        this.fileService.deleteFileByName(this.candidate.id, this.deletedCandidateDocument.id).subscribe(
          (res) => {
            this.getCandidateDocuments();
          },
          (err) =>{
            console.log(err);
          }
        );
      });
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-md'});
  }
 
  confirm(): void {
    this.candidateService.delete(this.candidate.id).then(item => {
      this.router.navigate(["/all-candidates"]);
    });
    this.modalRef.hide();
  }
 
  decline(): void {
    this.modalRef.hide();
  }


}
