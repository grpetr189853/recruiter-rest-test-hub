import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {Candidate} from '../../models/candidate';
import {CandidateService} from "../../services/candidate.service";
import {CitiesService} from "../../services/cities.service";
import {City} from "../../models/city";
import {ContactsCandidateService} from "../../services/contacts-candidate.service";
import {ContactsCandidate} from "../../models/contactsCandidate";
import {SkillsCandidateService} from "../../services/skills-candidate.service";
import {SkillsService} from "../../services/skills.service";
import { ImageService } from 'src/app/services/image.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SendCustomerModalComponent } from '../send-customer-modal/send-customer-modal.component';
import { SendCandidateModalComponent } from '../send-candidate-modal/send-candidate-modal.component';
import { ConnectCustomerModalComponent } from '../connect-customer-modal/connect-customer-modal.component';

@Component({
  selector: 'app-all-candidates',
  templateUrl: './all-candidates.component.html',
  styleUrls: ['./all-candidates.component.css']
})
export class AllCandidatesComponent implements OnInit {
  selectedCandidate: Candidate;
  candidates: Candidate[];
  city: City;
  candidateContacts: any = [];
  contactType: any;
  candidateSkills: any = [];
  tabStatus: number;
  searchString: string;
    /*****Candidate Statuses*****/
    readonly STATUS_ALL = 0;
    readonly STATUS_NEW = 1;
    readonly STATUS_CONSIDERED = 2;
    readonly STATUS_TODAY = 3;
    readonly STATUS_OFFER = 4;
    readonly STATUS_SELECTED = 5;
    readonly STATUS_BLACKLISTED = 6;    
  private candidateUrl = 'web/candidates?per-page=100000&page-count=44';
  constructor(
    private candidateService: CandidateService,
    private citiesService: CitiesService,
    private contactsCandidateService: ContactsCandidateService,
    private skillsCandidateService: SkillsCandidateService,
    private skillsService: SkillsService,
    private imageService: ImageService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    // this.getCandidates();
  }

  selectCandidatesStatuses(event, tabStatus: number) {
    event.preventDefault();
    this.tabStatus = tabStatus;
  }

  getStatuses() {
    return this.tabStatus;
  }

  searchCandidate(event) {
    this.searchString = event;
  }
}
