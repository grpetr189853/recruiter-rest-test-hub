import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {CKEditorComponent} from "@ckeditor/ckeditor5-angular";
import {ActivatedRoute, Params} from "@angular/router";
import { CitiesService } from 'src/app/services/cities.service';
import { City } from 'src/app/models/city';
import {switchMap} from "rxjs/operators";
import { SkillsService } from 'src/app/services/skills.service';
import { Skill } from 'src/app/models/skill';
import { CompanyService } from 'src/app/services/company.service';
import { Company } from 'src/app/models/company';
import { NgForm } from '@angular/forms';
import { Vacancy } from 'src/app/models/vacancy';
import { SkillsVacancyService } from 'src/app/services/skills-vacancy.service';
import { VacancyService } from 'src/app/services/vacancy.service';
import { VacancyDocumentsService } from 'src/app/services/vacancy-documents.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-add-vacancy',
  templateUrl: './add-vacancy.component.html',
  styleUrls: ['./add-vacancy.component.css']
})
export class AddVacancyComponent implements OnInit {
  @ViewChild('fileInputDocument', {static: false}) fileInputDocument:ElementRef;
  public Editor = ClassicEditor;
  cities: City[];
  skills: Skill[];
  companies: Company[];
  v_duties = '';
  w_conditions ='';
  v_requirements = '';
  v_description = '';
  vacancy: Vacancy;
  selectedSkills: any = [];
  selectedSkillsVacancy: any = [];
  vacancyDocuments: any = [];
  selectedFileDocument: FileSnippet;// attached file
  modalRef: BsModalRef;
  isVacancySaved: boolean;
  selectedVacancyId: string;
  /*Form inputs*/
  name: string;
  company: any;
  location: any;
  salary: number;
  constructor(
    private route: ActivatedRoute,
    private citiesService: CitiesService,
    private skillsService: SkillsService,
    private companyService: CompanyService,
    private vacancyService: VacancyService,
    private skillsVacancyService: SkillsVacancyService,
    private vacancyDocumentService: VacancyDocumentsService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.getCities();
    this.getSkills();
    this.getCompanies();
  }

  onFormSubmit(vacancyAddForm: NgForm) {
    console.log(vacancyAddForm);
    this.vacancy = new Vacancy();
    this.vacancy.name = vacancyAddForm.value.name;
    this.vacancy.company_id = vacancyAddForm.value.company;
    this.vacancy.location = vacancyAddForm.value.location;
    this.vacancy.salary = vacancyAddForm.value.salary;
    
    this.vacancy.v_duties = vacancyAddForm.value.v_duties;
    this.vacancy.w_conditions = vacancyAddForm.value.w_conditions;
    this.vacancy.v_requirements = vacancyAddForm.value.v_requirements;
    this.vacancy.v_description = vacancyAddForm.value.v_description;
    this.vacancyService.create(this.vacancy).then((vacancy) => {
      this.isVacancySaved = true;
      this.selectedVacancyId = vacancy.id;
      //Add selected skills
      this.uniqueSkillVacancy(this.selectedSkills).map(item => {
        this.selectedSkillsVacancy.push({skill_id : item.skill_id, vacancy_id: vacancy.id });
      });
      this.selectedSkillsVacancy.forEach(item => this.skillsVacancyService.addSkillVacancy(item));
      if(this.selectedFileDocument) {
        this.vacancyDocumentService.uploadFile(this.selectedFileDocument.file, vacancy.id).subscribe(
          (res) => {
  
          },
          (err) => {
  
        });
      }

    });
  }

  getCities() {
    this.route.params.pipe(switchMap((params: Params) => this.citiesService.getData()))
      .subscribe( cities => this.cities = cities);
  }

  getSkills() {
    this.route.params.pipe(switchMap((params: Params) => this.skillsService.getData()))
      .subscribe( skills => {
        this.skills = skills;
      });
  }

  getCompanies() {
    this.route.params.pipe(switchMap((params: Params) => this.companyService.getAllCompanies()))
      .subscribe(companies => {
        this.companies = companies;
      })
  }

    // Unique elements of array
    uniqueSkillVacancy(arr) {
      const result = [];
  
      for (const el of arr) {
        if (!result.some((e) => {
          return e.skill_id === el.skill_id && e.candidate_id === el.candidate_id;
        })) {
          result.push(el);
        }
      }
  
      return result;
    }

    addSkill(event) {
      event.value.map(item => this.selectedSkills.push({skill_id: item.id}));
    }

    processFile(fileInput: any) {
      const file: File = fileInput.files[0];
      const reader = new FileReader();

      reader.addEventListener('load', (event: any) => {
        this.fileInputDocument.nativeElement.value = null;
        this.selectedFileDocument = new FileSnippet(event.target.result, file);
        this.vacancyDocuments.push(this.selectedFileDocument);
      });
      reader.readAsDataURL(file);
    }

    removeVacancyDocument(event, el) {
      event.preventDefault();
      const deletedDocumentId = el.getAttribute('data-document-id');
      this.vacancyDocuments.splice(deletedDocumentId, 1);
    }

    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template, {class: 'modal-md'});
    }

    decline(): void {
      this.modalRef.hide();
    }

    confirm(): void {
      this.vacancyService.delete(this.selectedVacancyId).then(item => {
        this.name = undefined;
        this.company = undefined;
        this.location = undefined;
        this.salary = undefined;
        this.v_duties = '';
        this.w_conditions = '';
        this.v_requirements = '';
        this.v_description = '';
        this.skills = [];
        this.isVacancySaved = false;
        this.getSkills();
        this.fileInputDocument.nativeElement.value = null;
        this.selectedFileDocument = undefined;
        this.vacancyDocuments = [];
        /*
        this.vacancy.id = undefined;
        this.vacancy = undefined;
        this.vacancy.name = undefined;
        this.vacancy.location = undefined;
        this.vacancy.company_id = undefined;
        this.vacancy.salary = undefined;
        // this.vacancy.skills = [];
        this.selectedSkills = [];
        this.selectedSkillsVacancy = [];
        this.selectedFileDocument = undefined;
        this.vacancyDocuments = [];
        this.isVacancySaved = false;
        this.getSkills();
        this.fileInputDocument.nativeElement.value = null;
        */
      });
      this.modalRef.hide();
    }

}
