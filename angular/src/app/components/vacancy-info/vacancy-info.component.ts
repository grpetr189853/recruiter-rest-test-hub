import { Component, OnInit, OnChanges, SimpleChange, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { EventBusService } from 'src/app/services/shared/event-bus.service';
import { Vacancy } from 'src/app/models/vacancy';
import { City } from 'src/app/models/city';
import { CitiesService } from 'src/app/services/cities.service';
import { Company } from 'src/app/models/company';
import { CompanyService } from 'src/app/services/company.service';
import { VacancyUpdateComponent } from '../vacancy-update/vacancy-update.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SkillsVacancyService } from 'src/app/services/skills-vacancy.service';
import { SkillsService } from 'src/app/services/skills.service';
import { VacancyDocumentsService } from 'src/app/services/vacancy-documents.service';
class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-vacancy-info',
  templateUrl: './vacancy-info.component.html',
  styleUrls: ['./vacancy-info.component.css']
})
export class VacancyInfoComponent implements OnInit, OnChanges {
  @ViewChild('fileInput', {static: false}) fileInput:ElementRef;
  selectedVacancy: Vacancy;
  city: City;
  company: Company;
  selectedCandidate: any;
  bsModalRef: any;
  vacancySkills: any = [];
  vacancyDocuments: any;
  selectedVacancyDocument: any;
  deletedVacancyDocument: any;
  uploadedFile: any;
  constructor(
    private eventBusService: EventBusService,
    private citiesService: CitiesService,
    private companyService: CompanyService,
    private modalService: BsModalService,
    private skillsVacancyService: SkillsVacancyService,
    private skillsService: SkillsService,
    private vacancyDocumentService: VacancyDocumentsService,
  ) { }

  ngOnInit() {
    this.eventBusService.on('selectedVacancy',(data: Vacancy) => {
      if(data) {
        this.selectedVacancy = data;
        this.getVacancyCity(this.selectedVacancy.location.toString());
        this.getVacancyCompany(this.selectedVacancy.company_id);
        this.getVacancySkills(this.selectedVacancy.id);
        this.getVacancyDocuments(this.selectedVacancy.id);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.eventBusService.on('selectedVacancy',(data: Vacancy) => {
      if(data) {
        this.selectedVacancy = data;
        this.getVacancyCity(this.selectedVacancy.location.toString());
        this.getVacancyCompany(this.selectedVacancy.company_id);
      }
    });
  }

  getVacancyCity(id: string) {
    this.citiesService.getDetail(id).then( city => this.city = city);
  }

  getVacancyCompany(company_id: string) {
    this.companyService.getDetail(company_id).then(
      company => {
        this.company = company;
      }
    )
  }

  getVacancySkills(id: string) {
    this.vacancySkills = [];
    this.skillsVacancyService.getAllVacanciesSkills(id).then(
      skillsVacancy => {
        skillsVacancy.relatedSkills.forEach(
          skillVacancy => this.skillsService.getDetail(skillVacancy.skill_id).then(
            skill => this.vacancySkills.push(skill.name)
          )
        );
      }
    )
  }

  openVacancyUpdateModal() {
    const initialState = {
      selectedVacancyId: this.selectedVacancy.id,
      selectedVacancy: this.selectedVacancy,
      title: 'Редактировать вакансию'
    };
    this.bsModalRef = this.modalService.show(VacancyUpdateComponent, {initialState});
    this.bsModalRef.content.selectedVacancyId = this.selectedVacancy.id;
  }

  getVacancyDocuments(vacancyId: string) {
    this.selectedVacancyDocument = undefined;
    this.vacancyDocumentService.getVacancyDocuments(vacancyId).then(
      documentsVacancy => {
        this.vacancyDocuments = documentsVacancy;
        if(!this.uploadedFile && this.vacancyDocuments[0]){
          this.selectedVacancyDocument = this.vacancyDocuments[0].file_name;
        } else if (this.uploadedFile){
          this.selectedVacancyDocument = this.uploadedFile.file.name;
          this.uploadedFile = undefined;
        }
      }
    )
  }

  previewDocument(event, filename: string) {
    event.preventDefault();
    this.selectedVacancyDocument = filename;
  }

  removeVacancyDocumentByName(event, fileName: string) {
    event.preventDefault();
    this.vacancyDocumentService.getVacancyDocuments(this.selectedVacancy.id)
      .then(documentVacancy =>  {
        this.deletedVacancyDocument = documentVacancy.filter(doc => doc.file_name === fileName ).pop();
        this.vacancyDocumentService.deleteFileByName(this.selectedVacancy.id, this.deletedVacancyDocument.id).subscribe(
          (res) => {
            this.getVacancyDocuments(this.selectedVacancy.id); 
          },
          (err) =>{
            console.log(err);
          }
        );
      }
    );
  }

  processFile(fileInput: any) {
    
    const file: File = fileInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.fileInput.nativeElement.value = null;
      this.uploadedFile = new FileSnippet(event.target.result, file);

      this.uploadedFile.pending = true;

      this.vacancyDocumentService.uploadFile(this.uploadedFile.file, this.selectedVacancy.id).subscribe(
        (res) => {
          this.selectedVacancyDocument = this.uploadedFile.file.name;
          this.getVacancyDocuments(this.selectedVacancy.id);
        },
        (err) => {
          console.log(err);
        });

    });

    reader.readAsDataURL(file);

  }

}
