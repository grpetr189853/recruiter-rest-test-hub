import { Component, OnInit, Output, EventEmitter, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
// import { debounceTime, map } from 'rxjs/operators'; 
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import { FormControl } from '@angular/forms';
// import 'rxjs/add/operator/debounceTime';
@Component({
  selector: 'app-search-candidate',
  templateUrl: './search-candidate.component.html',
  styleUrls: ['./search-candidate.component.css']
})
export class SearchCandidateComponent implements OnInit, AfterViewInit {
  searchString: string;
  @Output() search = new EventEmitter();
  @ViewChild('input',{static: false}) input: ElementRef;
  constructor() { }
  ngOnInit() {
  }


  ngAfterViewInit() {
      // server-side search
    fromEvent(this.input.nativeElement,'keyup')
      .pipe(
          // filter(Boolean),
          debounceTime(1500),
          distinctUntilChanged(),
          tap((text) => {
            this.search.emit(this.input.nativeElement.value)
          })
      )
      .subscribe();
  }

}
