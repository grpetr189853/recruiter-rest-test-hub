import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendCustomerModalComponent } from './send-customer-modal.component';

describe('SendCustomerModalComponent', () => {
  let component: SendCustomerModalComponent;
  let fixture: ComponentFixture<SendCustomerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendCustomerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendCustomerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
