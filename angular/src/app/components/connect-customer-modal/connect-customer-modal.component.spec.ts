import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectCustomerModalComponent } from './connect-customer-modal.component';

describe('ConnectCustomerModalComponent', () => {
  let component: ConnectCustomerModalComponent;
  let fixture: ComponentFixture<ConnectCustomerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectCustomerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectCustomerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
