import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import {CandidateService} from "../../services/candidate.service";
import {NgForm} from "@angular/forms";
import {Candidate} from "../../models/candidate";
import {ActivatedRoute, Params} from "@angular/router";
import {CitiesService} from "../../services/cities.service";
import {SkillsService} from "../../services/skills.service";
import {SkillsCandidateService} from "../../services/skills-candidate.service";
import {ImageService} from "../../services/image.service";
import {FileService} from "../../services/file.service";
import {switchMap} from "rxjs/operators";
import {City} from "../../models/city";
import {Skill} from "../../models/skill";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ContactsCandidateService} from "../../services/contacts-candidate.service";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
class ImageSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
class FileSnippet {
  pending: boolean = false;
  status: string = 'init';
  constructor(public src: string, public file: File) {}
}
@Component({
  selector: 'app-add-candidate',
  templateUrl: './add-candidate.component.html',
  styleUrls: ['./add-candidate.component.css']
})

export class AddCandidateComponent implements OnInit {
  @ViewChild('fileInputResume', {static: false}) fileInputResume:ElementRef;
  @ViewChild('fileInputDocument', {static: false}) fileInputDocument:ElementRef;
  @ViewChild('photoInput', {static: false}) photoInput:ElementRef;

  /*DOCUMENT TYPE*/
  readonly IS_RESUME = 1;
  readonly IS_DOCUMENT = 0;
  /*CONTAcT TYPE*/
  readonly CONTACT_TYPE_PHONE = '1';
  readonly CONTACT_TYPE_EMAIL = '2';
  readonly CONTACT_TYPE_SKYPE = '3';
  readonly CONTACT_TYPE_LINKEDIN = '4';
  readonly CONTACT_TYPE_GITHUB = '5';
  readonly CONTACT_TYPE_BITBUCKET = '6';
  id: string;
  candidate: Candidate;
  lastname: string;
  name: string;
  fathername: string;
  location: number;
  cities: City[];
  company: string;
  position: string;
  salary: number;
  wish: string;
  contacts: any = [];
  date_birth: string;
  skills: Skill[];
  notes = '';
  contact_type: string;
  selectedSkills: any = [];
  selectedSkillsCandidate: any = [];
  public Editor = ClassicEditor;
  selectedPhoto: ImageSnippet;
  selectedFile: FileSnippet;// resume
  selectedFileDocument: FileSnippet;// attached file
  candidateResume: any = undefined;
  candidateDocuments: any = [];
  fileType: number;
  isCandidateSaved: boolean;
  modalRef: BsModalRef;
  message: string;
  constructor(
    private candidateService: CandidateService,
    private route: ActivatedRoute,
    private citiesService: CitiesService,
    private skillsService: SkillsService,
    private skillsCandidateService: SkillsCandidateService,
    private contactsCandidateService: ContactsCandidateService,
    private imageService: ImageService,
    private fileService: FileService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.getCities();
    this.getSkills();
  }

  onFormSubmit(candidateAddForm: NgForm) {
    console.log(candidateAddForm);
    this.candidate = new Candidate();
    this.candidate.lastname = this.lastname;
    this.candidate.name = this.name;
    this.candidate.fathername = this.fathername;
    this.candidate.location = this.location;
    this.candidate.company = this.company;
    this.candidate.position = this.position;
    this.candidate.salary = this.salary;
    this.candidate.wish = this.wish;
    this.candidate.date_birth = this.date_birth;
    this.candidate.notes = this.notes;
    console.log(this.candidate);
    this.candidateService.create(this.candidate).then((candidate) => {
        this.isCandidateSaved = true;
        this.id = candidate.id;
        this.uniqueSkillCandidate(this.selectedSkills).map(item => {
          this.selectedSkillsCandidate.push({skill_id : item.skill_id, candidate_id: candidate.id });
        });
        this.selectedSkillsCandidate.forEach(item => this.skillsCandidateService.addSkillCandidate(item));
        this.contacts.forEach(item => {
        if (item.match(/^\+380\d{9}$/)) {
          this.contact_type = this.CONTACT_TYPE_PHONE;
        } else if(item.includes('@')) {
          this.contact_type = this.CONTACT_TYPE_EMAIL;
        } else if(item.match('^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$'))  {
          this.contact_type = this.CONTACT_TYPE_SKYPE;
        } else if(item.includes('linkedin'))  {
          this.contact_type = this.CONTACT_TYPE_LINKEDIN;
        } else if(item.includes('github')) {
          this.contact_type = this.CONTACT_TYPE_GITHUB;
        } else if(item.includes('bitbucket'))  {
          this.contact_type = this.CONTACT_TYPE_BITBUCKET;
        } else {
          this.contact_type = this.CONTACT_TYPE_PHONE;
        }
        this.contactsCandidateService.addContactCandidate({
            candidate_id: candidate.id,
            contact_type: this.contact_type,
            value: item,
          });
        });
        if(this.selectedPhoto) {
          this.imageService.uploadPhoto(this.selectedPhoto.file, candidate.id).subscribe(
            (res) => {

              console.log(res);

            },
            (err) => {

            });
        }
        if(this.selectedFile) {
          this.fileService.uploadFile(this.selectedFile.file, candidate.id, this.IS_RESUME).subscribe(
            (res) => {

            },
            (err) => {

            });
        } 
        if (this.selectedFileDocument) {
          this.fileService.uploadFile(this.selectedFileDocument.file, candidate.id, this.IS_DOCUMENT).subscribe(
            (res) => {

            },
            (err) => {

            });
        }
      }
    );
  }

  getCities() {
    this.route.params.pipe(switchMap((params: Params) => this.citiesService.getData()))
      .subscribe( cities => this.cities = cities);
  }

  getSkills() {
    this.route.params.pipe(switchMap((params: Params) => this.skillsService.getData()))
      .subscribe( skills => {
        this.skills = skills;
      });
  }

  addSkill(event) {
    event.value.map(item => this.selectedSkills.push({skill_id: item.id}));
  }

  // Unique elements of array
  uniqueSkillCandidate(arr) {
    const result = [];

    for (const el of arr) {
      if (!result.some((e) => {
        return e.skill_id === el.skill_id && e.candidate_id === el.candidate_id;
      })) {
        result.push(el);
      }
    }

    return result;
  }

  addContact(contact) {
    const contactValue = contact.value;
    this.contacts.push('');
  }

  removeContact(i) {
    this.contacts.splice(i, 1);
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  processPhoto(photoInput: any) {
    const file: File = photoInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.photoInput.nativeElement.value = null;
      this.selectedPhoto = new ImageSnippet(event.target.result, file);
      this.selectedPhoto.pending = true;

      }
    );

    reader.readAsDataURL(file);
  }

  processFile(fileInput: any, fileType: number) {
    const file: File = fileInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.fileInputResume.nativeElement.value = null;
      this.fileInputDocument.nativeElement.value = null;
      if (fileType === this.IS_RESUME) {
        this.selectedFile = new FileSnippet(event.target.result, file);
        this.fileType = this.IS_RESUME;
      } else if (fileType === this.IS_DOCUMENT) {
        this.selectedFileDocument = new FileSnippet(event.target.result, file);
        this.fileType = this.IS_DOCUMENT;
        this.candidateDocuments.push(this.selectedFileDocument);
      }
    });
    reader.readAsDataURL(file);
  }

  removePhoto() {
    this.selectedPhoto = undefined;
  }

  removeFile() {
    this.selectedFile = undefined;
  }

  removeCandidateDocument(event, el) {
    event.preventDefault();
    const deletedDocumentId = el.getAttribute('data-document-id');
    this.candidateDocuments.splice(deletedDocumentId, 1);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-md'});
  }
 
  confirm(): void {
    this.candidateService.delete(this.id).then(item => {
      this.id = undefined;
      this.candidate = undefined;
      this.lastname = undefined;
      this.name = undefined;
      this.fathername = undefined;
      this.location = undefined;
      this.company = undefined;
      this.position = undefined;
      this.salary = undefined;
      this.wish = undefined;
      this.skills = [];
      this.contacts = [];
      this.selectedSkills = [];
      this.selectedSkillsCandidate = [];
      this.date_birth = undefined;
      this.notes = '';
      this.selectedPhoto = undefined;
      this.selectedFile = undefined;
      this.selectedFileDocument = undefined;
      this.candidateDocuments = [];
      this.isCandidateSaved = false;
      this.getSkills();
      this.fileInputResume.nativeElement.value = null;
      this.fileInputDocument.nativeElement.value = null;
      this.photoInput.nativeElement.value = null;
    });
    this.modalRef.hide();
  }
 
  decline(): void {
    this.modalRef.hide();
  }

}
