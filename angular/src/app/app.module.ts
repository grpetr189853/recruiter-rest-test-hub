import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CandidateComponent } from './components/candidate/candidate.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';
import { PagerService } from './index';
import { DetailComponent } from './components/detail/detail.component';
import {APP_BASE_HREF} from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import { AllCandidatesComponent } from './components/all-candidates/all-candidates.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS} from './date_adapters/date.adapter';
import { CandidateUpdateComponent } from './components/candidate-update/candidate-update.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import {NgxExtendedPdfViewerModule} from "ngx-extended-pdf-viewer";
import { AddCandidateComponent } from './components/add-candidate/add-candidate.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SendCustomerModalComponent } from './components/send-customer-modal/send-customer-modal.component';
import { SendCandidateModalComponent } from './components/send-candidate-modal/send-candidate-modal.component';
import { ConnectCustomerModalComponent } from './components/connect-customer-modal/connect-customer-modal.component';
import { CandidateListComponent } from './components/candidate-list/candidate-list.component';
import { CandidateInfoComponent } from './components/candidate-info/candidate-info.component';
import { SearchCandidateComponent } from './components/search-candidate/search-candidate.component';
import { SearchPipePipe } from './pipes/search-pipe.pipe';
import { SelectedCandidatePipe } from './pipes/selected-candidate.pipe';
import { AllVacanciesComponent } from './components/all-vacancies/all-vacancies.component';
import { VacancyListComponent } from './components/vacancy-list/vacancy-list.component';
import { VacancyInfoComponent } from './components/vacancy-info/vacancy-info.component';
import { VacancyUpdateComponent } from './components/vacancy-update/vacancy-update.component';
import { SafehtmlPipe } from './pipes/safehtml.pipe';
import { AddVacancyComponent } from './components/add-vacancy/add-vacancy.component';

const routes: Routes = [
  // basic routes
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: CandidateComponent },
  { path: 'all-candidates', component: AllCandidatesComponent},
];
@NgModule({
  declarations: [
    AppComponent,
    CandidateComponent,
    DetailComponent,
    AllCandidatesComponent,
    CandidateUpdateComponent,
    AddCandidateComponent,
    SendCustomerModalComponent,
    SendCandidateModalComponent,
    ConnectCustomerModalComponent,
    CandidateListComponent,
    CandidateInfoComponent,
    SearchCandidateComponent,
    SearchPipePipe,
    SelectedCandidatePipe,
    AllVacanciesComponent,
    VacancyListComponent,
    VacancyInfoComponent,
    VacancyUpdateComponent,
    SafehtmlPipe,
    AddVacancyComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    RouterModule.forRoot(routes),
    AngularFontAwesomeModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    CKEditorModule,
    NgxExtendedPdfViewerModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
  ],
  entryComponents: [
    SendCustomerModalComponent,
    SendCandidateModalComponent,
    ConnectCustomerModalComponent,
    VacancyUpdateComponent,
  ],
  providers: [
    PagerService,
    {provide: APP_BASE_HREF, useValue : '/' },
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
