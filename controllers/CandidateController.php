<?php


namespace app\controllers;

use app\crm\entities\candidate\Candidate;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use app\crm\entities\candidate\DocumentCandidate;
use yii\web\NotFoundHttpException;

class CandidateController extends ActiveController
{

    public $dto;

    public $service;

    public $modelClass = 'app\crm\entities\candidate\Candidate';

    public $updateScenario = Candidate::SCENARIO_UPDATE;

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function init() {
        $this->service = \Yii::$container->get('CandidateService');
        $this->dto = \Yii::$container->get('candidateDTO');
        parent::init();
    }

    public function actions(){
        $actions = parent::actions();
        unset($actions['index'], $actions['create'], $actions['delete']);
        $actions['update']['scenario'] = Candidate::SCENARIO_UPDATE;
        $actions['default']['scenario'] = Candidate::SCENARIO_DEFAULT;
        return $actions;
    }

    public function actionIndex(){
        $activeData = new ActiveDataProvider([
            'query' => Candidate::find(),
            'pagination' => [
                'defaultPageSize' => -1,
                'pageSizeLimit' => -1,
            ],
        ]);
        return $activeData;
    }

    public function actionUploadPhoto() {
        $model = new $this->modelClass;
        if($model->validate()) {
            $model->photo = \yii\web\UploadedFile::getInstanceByName('photo');
            if($model->photo) {
                $extension = pathinfo($model->photo, PATHINFO_EXTENSION);
                $m = Candidate::findOne(['id' => \Yii::$app->request->get('id')]);
                $name = uniqid() . '_' . date_timestamp_get(date_create()) . '.' . $extension;
                $path = '/files/photo/' . $name;
                $m->photo = $name;
                if ($model->photo->saveAs(\Yii::getAlias('@app') . $path)) {
                    $m->save();
                }
                return $m;
            }
        }
        return false;
    }

    public function actionDeletePhoto() {
        $model = Candidate::findOne(['id' => \Yii::$app->request->get('id')]);
        $path = '/files/photo/' . $model->photo;
        if(is_file(\Yii::getAlias('@app') . $path)) {
            unlink(\Yii::getAlias('@app') . $path);
        }
        $model->photo = null;
        $model->save();
        return $model;
    }

    public function actionCreate() {
        $params['lastname'] = \Yii::$app->request->getBodyParam('lastname');
        $params['name'] = \Yii::$app->request->getBodyParam('name');
        $params['fathername'] = \Yii::$app->request->getBodyParam('fathername');
        $params['location'] = \Yii::$app->request->getBodyParam('location');
        $params['salary'] = \Yii::$app->request->getBodyParam('salary');
        $params['current_company'] = \Yii::$app->request->getBodyParam('company');
        $params['current_position'] = \Yii::$app->request->getBodyParam('position');
        $params['skills'] = null;
        $params['documents'] = null;
        $params['photo'] = null;
        $params['wish'] = \Yii::$app->request->getBodyParam('wish');
        $params['comment'] = \Yii::$app->request->getBodyParam('notes');
        $this->dto->load($params);
        $this->service->create($this->dto);
        return $this->dto;
    }

    public function actionDelete() {
        $candidate_id = \Yii::$app->request->get('id');
        try {
            $model = Candidate::findOne(['id' => $candidate_id]);
            $path = '/files/photo/' . $model->photo;
            if(is_file(\Yii::getAlias('@app') . $path)) {
                unlink(\Yii::getAlias('@app') . $path);
            }    
        } catch (\Exception $e) {
            throw new NotFoundHttpException('Failed to delete candidate photo for unknown reason');
        }

        try {
            $modelDocumentsCandidate = DocumentCandidate::find()->where('entity_id = :candidate_id', [':candidate_id' => $candidate_id])->all();
            foreach ($modelDocumentsCandidate as $document) {
                $path = '/files/attachments/' . $candidate_id . '/' . $document->file_name;
                if(is_file(\Yii::getAlias('@app') . $path)) {
                    unlink(\Yii::getAlias('@app') . $path);
                }
            }
        } catch (\Exception $e) {
            throw new NotFoundHttpException('Failed to delete candidate document for unknown reason');
        }

        $model->delete();
    }

}