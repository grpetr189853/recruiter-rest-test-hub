<?php


namespace app\controllers;

use app\crm\entities\candidate\StatusCandidates;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class StatusesCandidateController extends ActiveController
{
    public $modelClass = 'app\crm\entities\candidate\StatusCandidates';
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex()
    {
        $candidate_id = \Yii::$app->request->get('candidate_id');
        $activeData = new ActiveDataProvider([
            'query' => StatusCandidates::find()->where('candidate_id = :candidate_id', [':candidate_id' => $candidate_id]),
            'pagination' => [
                'defaultPageSize' => -1,
                'pageSizeLimit' => -1,
            ],
        ]);
        return $activeData;
    }
}