<?php


namespace app\controllers;

use app\crm\entities\vacancy\Vacancy;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use app\crm\entities\vacancy\DocumentVacancy;

class VacancyController extends ActiveController
{

    public $dto;

    public $service;

    public $modelClass = 'app\crm\entities\vacancy\Vacancy';

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function init() {
        $this->service = \Yii::$container->get('VacancyService');
        $this->dto = \Yii::$container->get('vacancyDTO');
        parent::init();
    }

    public function actions(){
        $actions = parent::actions();
        unset($actions['index'], $actions['create'], $actions['delete']);
        $actions['update']['scenario'] = Vacancy::SCENARIO_UPDATE;
        return $actions;
    }

    public function actionIndex()
    {
        $activeData = new ActiveDataProvider([
            'query' => Vacancy::find(),
            'pagination' => [
                'defaultPageSize' => -1,
                'pageSizeLimit' => -1,
            ],
        ]);
        return $activeData;
    }

    public function actionCreate() {
        $params['name'] = \Yii::$app->request->getBodyParam('name');
        $params['location'] = \Yii::$app->request->getBodyParam('location');
        $params['salary'] = \Yii::$app->request->getBodyParam('salary');
        $params['company'] = \Yii::$app->request->getBodyParam('company_id');
        $params['customers'] = 'd56644ae-a359-4761-aa10-9d8c52d52daa';
        $params['skills'] = null;
        $params['documents'] = null;
        $params['v_duties'] = \Yii::$app->request->getBodyParam('v_duties');
        $params['w_conditions'] = \Yii::$app->request->getBodyParam('w_conditions');
        $params['v_requirements'] = \Yii::$app->request->getBodyParam('v_requirements');
        $params['v_description'] = \Yii::$app->request->getBodyParam('v_description');
        $this->dto->load($params);
        $this->service->create($this->dto);
        return $this->dto;
    }

    public function actionDelete() {
        $vacancy_id = \Yii::$app->request->get('id');

        try {
            $model = Vacancy::findOne(['id' => $vacancy_id]);
            $modelDocumentsVacancy = DocumentVacancy::find()->where('vacancy_id = :vacancy_id', [':vacancy_id' => $vacancy_id])->all();
            foreach ($modelDocumentsVacancy as $document) {
                $path = '/files/attachments/' . $vacancy_id . '/' . $document->file_name;
                if(is_file(\Yii::getAlias('@app') . $path)) {
                    unlink(\Yii::getAlias('@app') . $path);
                }
            }
        } catch (\Exception $e) {
            throw new NotFoundHttpException('Failed to delete candidate document for unknown reason');
        }
        $model->delete();
    }
}