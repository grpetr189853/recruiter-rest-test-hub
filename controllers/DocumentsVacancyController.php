<?php


namespace app\controllers;

use app\crm\entities\vacancy\DocumentVacancy;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
class DocumentsVacancyController extends ActiveController
{
    public $modelClass = 'app\crm\entities\vacancy\DocumentVacancy';
    public $updateScenario = DocumentVacancy::SCENARIO_UPDATE;
    public $createScenario = DocumentVacancy::SCENARIO_CREATE;
    public $deleteScenario = DocumentVacancy::SCENARIO_DELETE;

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function actions(){
        $actions = parent::actions();
        $actions['create']['scenario'] = DocumentVacancy::SCENARIO_CREATE;
        $actions['update']['scenario'] = DocumentVacancy::SCENARIO_UPDATE;
        unset($actions['index'],$actions['create'],$actions['update'],$actions['delete']);
        return $actions;
    }

    public function actionIndex()
    {
        $vacancy_id = \Yii::$app->request->get('vacancy_id');
        $activeData = new ActiveDataProvider([
            'query' => DocumentVacancy::find()->where('vacancy_id = :vacancy_id', [':vacancy_id' => $vacancy_id]),
            'pagination' => [
                'defaultPageSize' => -1,
                'pageSizeLimit' => -1,
            ],
        ]);
        return $activeData;
    }

    public function actionCreate()
    {
        $vacancy_id = \Yii::$app->request->get('vacancy_id');
        $model = new $this->modelClass([
            'scenario' => DocumentVacancy::SCENARIO_CREATE,
        ]);
        if($model->validate()) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');
            $path = '/files/attachments/' . $vacancy_id . '/' . $file->name;
            if (!file_exists(\Yii::getAlias('@app/files/attachments/') . $vacancy_id)) {
                mkdir(\Yii::getAlias('@app/files/attachments/') . $vacancy_id, 0755, true);
            }
            $model->file_name = $file->name;
            $model->file_extension = $file->extension;
            $model->vacancy_id = $vacancy_id;
            // $model->is_resume = \Yii::$app->request->headers->get('file-type');
            if ($file->saveAs(\Yii::getAlias('@app') . $path)) {
                $model->save();
            }
        }
    }

    public function actionDelete() {

        $vacancy_id = \Yii::$app->request->get('vacancy_id');
        try {
            $model = DocumentVacancy::find()->where('vacancy_id = :vacancy_id', [':vacancy_id' => $vacancy_id])->one();
            $model->scenario = DocumentVacancy::SCENARIO_DELETE;
            $path = '/files/attachments/' . $vacancy_id . '/' . $model->file_name;
            if(is_file(\Yii::getAlias('@app') . $path)) {
                unlink(\Yii::getAlias('@app') . $path);
            }
            $model->delete();
        }
        catch (\Exception $e) {
            throw new NotFoundHttpException('Vacancy document not found');
        }


        return $model;
    }

}