<?php


namespace app\controllers;

use app\crm\entities\candidate\DocumentCandidate;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
class DocumentsCandidateController extends ActiveController
{
    public $modelClass = 'app\crm\entities\candidate\DocumentCandidate';
    public $updateScenario = DocumentCandidate::SCENARIO_UPDATE;
    public $createScenario = DocumentCandidate::SCENARIO_CREATE;
    public $deleteScenario = DocumentCandidate::SCENARIO_DELETE;

    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function actions(){
        $actions = parent::actions();
        $actions['create']['scenario'] = DocumentCandidate::SCENARIO_CREATE;
        $actions['update']['scenario'] = DocumentCandidate::SCENARIO_UPDATE;
        unset($actions['index'],$actions['create'],$actions['update'],$actions['delete']);
        return $actions;
    }

    public function actionIndex()
    {
        $candidate_id = \Yii::$app->request->get('candidate_id');
        $activeData = new ActiveDataProvider([
            'query' => DocumentCandidate::find()->where('entity_id = :candidate_id', [':candidate_id' => $candidate_id]),
            'pagination' => [
                'defaultPageSize' => -1,
                'pageSizeLimit' => -1,
            ],
        ]);
        return $activeData;
    }

    public function actionCreate()
    {
        $candidate_id = \Yii::$app->request->get('candidate_id');
        $model = new $this->modelClass([
            'scenario' => DocumentCandidate::SCENARIO_CREATE,
        ]);
        if($model->validate()) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');
            $path = '/files/attachments/' . $candidate_id . '/' . $file->name;
            if (!file_exists(\Yii::getAlias('@app/files/attachments/') . $candidate_id)) {
                mkdir(\Yii::getAlias('@app/files/attachments/') . $candidate_id, 0755, true);
            }
            $model->file_name = $file->name;
            $model->file_extension = $file->extension;
            $model->entity_id = $candidate_id;
            $model->is_resume = \Yii::$app->request->headers->get('file-type');
            if ($file->saveAs(\Yii::getAlias('@app') . $path)) {
                $model->save();
            }
        }
    }

    public function actionDelete() {
        if(\Yii::$app->request->headers->get('file-type') == DocumentCandidate::IS_RESUME){
            $candidate_id = \Yii::$app->request->get('candidate_id');
            try {
                $model = DocumentCandidate::find()->where('entity_id = :candidate_id', [':candidate_id' => $candidate_id])->one();
                $model->scenario = DocumentCandidate::SCENARIO_DELETE;
                $path = '/files/attachments/' . $candidate_id . '/' . $model->file_name;
                if(is_file(\Yii::getAlias('@app') . $path)) {
                    unlink(\Yii::getAlias('@app') . $path);
                }
                $model->delete();
            }
            catch (\Exception $e) {
                throw new NotFoundHttpException('Resume not found');
            }
        } else if (\Yii::$app->request->headers->get('file-type') == DocumentCandidate::IS_DOCUMENT) {
            try{
                $candidate_id = \Yii::$app->request->get('candidate_id');
                $model = DocumentCandidate::find()->where('entity_id = :candidate_id', [':candidate_id' => $candidate_id])->one();
                $document_id = \Yii::$app->request->get('id');
                $candidateDocumentModel = DocumentCandidate::find()->where('id = :id',[':id' => $document_id])->one();
                $candidateDocumentModel->scenario = DocumentCandidate::SCENARIO_DELETE;
                $path = '/files/attachments/' . $candidate_id . '/' . $candidateDocumentModel->file_name;
                if(is_file(\Yii::getAlias('@app') . $path)) {
                    unlink(\Yii::getAlias('@app') . $path);
                }
                $candidateDocumentModel->delete();
            }catch(\Exception $e){
                throw new NotFoundHttpException('Document not found');
            }
        }

        return $model;
    }

}