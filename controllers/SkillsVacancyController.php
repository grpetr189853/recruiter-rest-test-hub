<?php


namespace app\controllers;

use app\crm\entities\vacancy\SkillVacancy;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
class SkillsVacancyController extends ActiveController
{
    public $modelClass = 'app\crm\entities\vacancy\SkillVacancy';
    public $updateScenario = SkillVacancy::SCENARIO_UPDATE;
    public $createScenario = SkillVacancy::SCENARIO_CREATE;
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
            ],
        ]);
    }

    public function actions(){
        $actions = parent::actions();
        $actions['create']['scenario'] = Skillvacancy::SCENARIO_CREATE;
        $actions['update']['scenario'] = SkillVacancy::SCENARIO_UPDATE;
        unset($actions['index'],$actions['create']);
        return $actions;
    }

    public function actionIndex()
    {
      $vacancy_id = \Yii::$app->request->get('vacancy_id');
      $activeData = new ActiveDataProvider([
          'query' => SkillVacancy::find()->where('vacancy_id = :vacancy_id', [':vacancy_id' => $vacancy_id]),
          'pagination' => [
              'defaultPageSize' => -1,
              'pageSizeLimit' => -1,
          ],
      ]);
      return $activeData;
    }

    public function actionCreate()
    {
        $vacancy_id = \Yii::$app->request->get('vacancy_id');
        $model = new $this->modelClass([
            'scenario' => SkillVacancy::SCENARIO_CREATE,
        ]);

        $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            $response = \Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['actionView', 'id' => $vacancy_id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

}