<?php

use yii\db\Migration;

/**
 * Class m200526_043422_additional_statuses
 */
class m200526_043422_additional_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%additional_statuses}}', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->char(36)->notNull(),
            'status' => $this->integer()->defaultValue(0),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%additional_statuses}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200526_043422_additional_statuses cannot be reverted.\n";

        return false;
    }
    */
}
