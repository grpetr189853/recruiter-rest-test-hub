<?php

use yii\db\Migration;

/**
 * Class m200413_083220_change_contact_type_table
 */
class m200413_083220_change_contact_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('contact_type', ['name' => 'GITHUB'], ['id' => '5']);
        $this->update('contact_type', ['name' => 'BITBUCKET'], ['id' => '6']);
        $this->delete('contact_type',['id' => '7']);
        $this->delete('contact_type',['id' => '8']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('contact_type', ['name' => 'PHONE'], ['id' => '5']);
        $this->update('contact_type', ['name' => 'EMAIL'], ['id' => '6']);
        $this->batchInsert('contact_type', ['name'], [
            ['SKYPE'],
            ['LINKEDIN'],
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200413_083220_change_contact_type_table cannot be reverted.\n";

        return false;
    }
    */
}
