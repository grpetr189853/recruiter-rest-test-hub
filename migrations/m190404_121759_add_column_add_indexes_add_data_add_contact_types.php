<?php

use yii\db\Migration;

/**
 * Class m190404_121759_add_column_add_indexes_add_data_add_contact_types
 */
class m190404_121759_add_column_add_indexes_add_data_add_contact_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%countries}}',
            ['name'],
            [['Украина']]
        );

        $this->batchInsert('{{%cities}}',
            ['country_id','name'],
            [
                ['1','Киев'],
                ['1','Харьков'],
                ['1','Львов'],
                ['1','Одесса'],
                ['1','Днепр'],
            ]
        );

        $this->batchInsert('{{%contact_type}}',
            ['name'],
            [
                ['PHONE'],
                ['EMAIL'],
                ['SKYPE'],
                ['LINKEDIN'],
            ]
        );

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%statuses}}', [
            'id'        => $this->primaryKey(),
            'name'      => $this->string()->notNull()
        ], $tableOptions);

        $this->batchInsert('{{%statuses}}',
            ['name'],
            [
                ['Новые'],
                ['На рассмотрении'],
                ['Сегодня'],
                ['Оффер'],
                ['Отобранные'],
                ['Черный список'],
            ]
        );

        $this->execute("ALTER TABLE `skills_candidate` ADD UNIQUE INDEX `index_candidate_skills_unique_twice` (`skill_id`, `candidate_id`) USING BTREE;");

        $this->addColumn('{{%candidates}}', 'photo', $this->text());
        $this->addColumn('{{%candidates}}', 'wish', $this->text());
        $this->addColumn('{{%candidates}}', 'documents', $this->json());
        $this->addColumn('{{%candidates}}', 'notes', $this->json());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%candidates}}', 'photo');
        $this->dropColumn('{{%candidates}}', 'wish');
        $this->dropColumn('{{%candidates}}', 'documents');
        $this->dropColumn('{{%candidates}}', 'notes');
        $this->dropIndex('index_candidate_skills_unique_twice','{{%skills_candidate}}');
        $this->dropTable('{{%statuses}}');
    }


}
