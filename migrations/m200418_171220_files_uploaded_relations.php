<?php

use yii\db\Migration;

/**
 * Class m200418_171220_files_uploaded_relations
 */
class m200418_171220_files_uploaded_relations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%files_uploaded}}','entity_id',$this->char(36)->notNull());
        $this->execute('ALTER TABLE `files_uploaded` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci'); 
        $this->createIndex(
            'index-files_uploaded-entity_id',
            '{{%files_uploaded}}',
            'entity_id');

        $this->addForeignKey(
            'fk-files_uploaded-candidate',
            '{{%files_uploaded}}',
            'entity_id',
            '{{%candidates}}',
            'id',
            'CASCADE',
            'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-files_uploaded-candidate','{{%files_uploaded}}');
        $this->dropIndex('index-files_uploaded-entity_id','{{%files_uploaded}}');
    }

}
