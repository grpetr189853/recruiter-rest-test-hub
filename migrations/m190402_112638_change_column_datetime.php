<?php

use yii\db\Migration;

/**
 * Class m190402_112638_change_column_datetime
 */
class m190402_112638_change_column_datetime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `candidates`
	CHANGE COLUMN `date_birth` `date_birth` DATE NULL DEFAULT NULL AFTER `salary`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("ALTER TABLE `candidates`
	CHANGE COLUMN `date_birth` `date_birth` DATETIME NULL DEFAULT NULL AFTER `salary`;");
    }


}
