<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%additional_status}}`.
 */
class m191106_085632_create_additional_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%additional_status}}', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->char(36)->notNull(),
            'new' => $this->integer()->defaultValue(0),
            'considered' => $this->integer()->defaultValue(0),
            'today' => $this->integer()->defaultValue(0),
            'offer' => $this->integer()->defaultValue(0),
            'selected' => $this->integer()->defaultValue(0),
            'blacklisted' => $this->integer()->defaultValue(0),
        ], $tableOptions);
        /*
        $this->addForeignKey(
            'fk_candidate_id',
            'additional_status',
            'candidate_id',
            'candidates',
            'id',
            'CASCADE'
        );
        */
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%additional_status}}');
    }
}
