<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contacts_customer}}`.
 */
class m190827_182235_create_contacts_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contacts_customer}}', [
            'id'    => $this->primaryKey(),
            'customer_id'   => $this->char(36)->notNull(),
            'contact_type'  => $this->integer()->notNull(),
            'value' => $this->string()->null(),
        ], $tableOptions);

        $this->createIndex(
            'index-customer_contacts-customer_id',
            '{{%contacts_customer}}',
            'customer_id');
        $this->addForeignKey(
            'fk-customer_contacts-customer',
            '{{%contacts_customer}}',
            'customer_id',
            '{{%customers}}',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->createIndex(
            'index-customer_contacts-contact_type_id',
            '{{%contacts_customer}}',
            'contact_type');
        $this->addForeignKey(
            'fk-customer_contacts-contact_type',
            '{{%contacts_customer}}',
            'contact_type',
            '{{%contact_type}}',
            'id',
            'CASCADE',
            'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%contacts_customer}}','fk-customer_contacts-customer');
        $this->dropIndex('{{%contacts_customer}}', 'index-customer_contacts-customer_id');

        $this->dropForeignKey('{{%contacts_customer}}', 'fk-customer_contacts-contact_type');
        $this->dropIndex('{{%contacts_customer}}', 'index-customer_contacts-contact_type_id');

        $this->dropTable('{{%contacts_customer}}');
    }
}