<?php

use yii\db\Migration;
use Ramsey\Uuid\Uuid;
use app\crm\entities\City;
use app\crm\entities\customer\Customer;
use app\crm\entities\candidate\related\ContactType;
use app\crm\entities\candidate\Candidate;

/**
 * Class m190910_101705_insert_test_data_vacancies_customers
 */
class m190910_101705_insert_test_data_vacancies_customers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insertCustomers();

        $this->addContactsToCustomers();
        $this->addContactsToCustomers();

        $this->addContactsToCandidates();
        $this->addContactsToCandidates();

        $this->insertVacancies();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DELETE FROM contacts_customer");
        $this->execute("DELETE FROM contacts_candidate");
        $this->execute("DELETE FROM vacancies");
        $this->execute("DELETE FROM customers");
    }

    private function insertCustomers()
    {
        $cities = City::find()->all();

        $customers = [
            'tableName' => 'customers',
            'fields' => ['id', 'name', 'lastname', 'fathername', 'location', 'created_at', 'updated_at'],
            'data' => [
                ['id' => Uuid::uuid4()->toString(), 'name' => 'Customer_N1', 'lastname' => 'Customer_LN1', 'fathername' => 'Customer_FN1', 'location' => $this->getRandCityID($cities), 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['id' => Uuid::uuid4()->toString(), 'name' => 'Customer_N2', 'lastname' => 'Customer_LN2', 'fathername' => 'Customer_FN2', 'location' => $this->getRandCityID($cities), 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['id' => Uuid::uuid4()->toString(), 'name' => 'Customer_N3', 'lastname' => 'Customer_LN3', 'fathername' => 'Customer_FN3', 'location' => $this->getRandCityID($cities), 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['id' => Uuid::uuid4()->toString(), 'name' => 'Customer_N4', 'lastname' => 'Customer_LN4', 'fathername' => 'Customer_FN4', 'location' => $this->getRandCityID($cities), 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['id' => Uuid::uuid4()->toString(), 'name' => 'Customer_N5', 'lastname' => 'Customer_LN5', 'fathername' => 'Customer_FN5', 'location' => $this->getRandCityID($cities), 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
            ]
        ];

        $this->execute($this->createSQL($customers));
    }

    private function insertVacancies()
    {
        $customers = [
            'tableName' => 'vacancies',
            'fields' => ['name', 'created_at', 'updated_at'],
            'data' => [
                ['name' => 'Machine Learning Engineer', 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['name' => 'Mobile IOS Developer', 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['name' => 'Mobile Android Developer', 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['name' => 'FullStack Developer', 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
                ['name' => 'Embedded Developer', 'created_at' => 'UNIX_TIMESTAMP()', 'updated_at' => 'UNIX_TIMESTAMP()'],
            ]
        ];

        $this->execute($this->createSQL($customers));
    }

    private function addContactsToCustomers()
    {
        $contactsToCustomers = [
            'tableName' => 'contacts_customer',
            'fields' => ['customer_id', 'contact_type', 'value'],
            'data' => [ ]
        ];

        $customers = Customer::find()->all();
        $contacts = ContactType::find()->all();

        if(count($contacts) > 1) {
            foreach($customers as $customer) {
                $contact = $contacts[rand(0, count($contacts) - 1)];

                $value = strtolower($contact->name);

                switch($value) {
                    case 'email':
                        $value = ($customer->name . '@gmail.com');
                        break;
                    case 'phone':
                        $value = '+380500000000' . rand(0, 9);
                        break;
                    case 'skype':
                    case 'linkedin':
                    case 'github':
                        $value = ($customer->name . '.' . $value);
                        break;
                }

                $contactsToCustomers['data'][] = [
                    'customer_id' => $customer->id,
                    'contact_type' => $contact->id,
                    'value' => $value
                ];
            }
        }

        $this->execute($this->createSQL($contactsToCustomers));
    }

    private function addContactsToCandidates()
    {
        $contactsToCandidates = [
            'tableName' => 'contacts_candidate',
            'fields' => ['candidate_id', 'contact_type', 'value'],
            'data' => []
        ];

        $candidates = Candidate::find()->all();
        $contacts = ContactType::find()->all();

        if(count($contacts) > 1) {
            foreach($candidates as $candidate) {
                $contact = $contacts[rand(0, count($contacts) - 1)];

                $value = strtolower($contact->name);

                switch($value) {
                    case 'email':
                        $value = ($candidate->name . '@gmail.com');
                        break;
                    case 'phone':
                        $value = '+380500000000' . rand(0, 9);
                        break;
                    case 'skype':
                    case 'linkedin':
                    case 'github':
                        $value = ($candidate->name . '.' . $value);
                        break;
                }

                $contactsToCandidates['data'][] = [
                    'candidate_id' => $candidate->id,
                    'contact_type' => $contact->id,
                    'value' => $value
                ];
            }
        }

        $this->execute($this->createSQL($contactsToCandidates));
    }

    private function createSQL($config = [])
    {
        $fieldsDates = ['created_at', 'updated_at'];

        $sql = "";

        $fields = join(', ', array_map(function($field) {
            return ('`' . $field . '`');
        }, $config['fields']));

        $values = [];

        foreach($config['fields'] as $field) {
            foreach($config['data'] as $index => $item) {
                $value = null;

                if(!empty($item[$field])) {
                    $value = $item[$field];

                    if(!in_array($field, $fieldsDates)) {
                        $value = "'" . $value . "'";
                    }
                }

                if(empty($values[$index])) {
                    $values[$index] = [];
                }

                $values[$index][$field] = $value;
            }
        }

        $values = array_map(function($value) {
           return '(' . join(', ', array_values($value)) . ')';
        }, $values);

        $values = 'VALUES ' . join(', ', $values);

        if(!empty($fields) && !empty($values)) {
            $sql .= "INSERT INTO " . $config['tableName'] . "(". $fields .")" . " " . $values;
        }

        return $sql;
    }

    private function getRandCityID($cities = [])
    {
        $cityID = 0;

        if(count($cities) > 1) {
            $cityID = $cities[rand(0, count($cities) - 1)]->id;
        }

        return $cityID;
    }
}
