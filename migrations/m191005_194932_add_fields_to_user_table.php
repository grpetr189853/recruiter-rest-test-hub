<?php

use yii\db\Migration;

/**
 * Class m191006_143042_add_fields_to_user_table
 */
class m191005_194932_add_fields_to_user_table extends Migration
{
    private $tableName = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'name', $this->string(255)->notNull());
        $this->addColumn($this->tableName, 'lastname', $this->string(255)->notNull());
        $this->addColumn($this->tableName, 'date_birth', $this->integer(11)->null());
        $this->addColumn($this->tableName, 'location_id', $this->integer(11)->null());
        $this->addColumn($this->tableName, 'status', $this->integer(11)->notNull());

        $this->alterColumn($this->tableName, 'created_at', $this->timestamp()->notNull());
        $this->alterColumn($this->tableName, 'updated_at', $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()'))->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropcolumn('$this->tableName', 'name');
        $this->dropcolumn('$this->tableName', 'lastname');
        $this->dropcolumn('$this->tableName', 'date_birth');
        $this->dropcolumn('$this->tableName', 'location_id');
        $this->dropcolumn('$this->tableName', 'status');

        $this->alterColumn($this->tableName, 'created_at', $this->integer(11)->notNull());
        $this->alterColumn($this->tableName, 'updated_at', $this->integer(11)->notNull());
    }
}
