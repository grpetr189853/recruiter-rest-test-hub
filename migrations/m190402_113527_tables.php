<?php

use yii\db\Migration;

/**
 * Class m190402_113527_tables
 */
class m190402_113527_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }


            $this->createTable('{{%countries}}', [
                'id'        => $this->primaryKey(),
                'name'      => $this->string()->notNull()
            ], $tableOptions);

            $this->createTable('{{%cities}}', [
                'id'        => $this->primaryKey(),
                'country_id'=> $this->integer(),
                'name'      => $this->string()->notNull()
            ], $tableOptions);

            $this->createTable('{{%skills}}', [
                'id'        => $this->primaryKey(),
                'name'      => $this->string()->notNull()
            ], $tableOptions);

            $this->createTable('{{%stages}}', [
                'id'        => $this->primaryKey(),
                'name'      => $this->string()->notNull()
            ], $tableOptions);

            $this->createTable('{{%skills_candidate}}', [
                'id'        => $this->primaryKey(),
                'skill_id'  => $this->integer()->notNull(),
                'candidate_id'=> $this->char(36)->notNull()
            ], $tableOptions);

            $this->createTable('{{%stages_candidate}}', [
                'id'        => $this->primaryKey(),
                'stage_id'  => $this->integer()->notNull(),
                'candidate_id'=> $this->char(36)->notNull(),
                'vacancy_id'=> $this->char(36)->notNull()
            ], $tableOptions);

            $this->createTable('{{%notes}}', [
                'id'        => $this->primaryKey(),
                'user_id'   => $this->integer()->notNull(),
                'date'      => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'text'      =>  $this->text()
            ], $tableOptions);

            $this->createTable('{{%documents}}', [
                'id'        => $this->primaryKey(),
                'user_id'   => $this->integer()->notNull(),
                'date'      => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
                'filename'  => $this->string()->notNull(),
                'file_ext'  => $this->string(5)->notNull(),
                'status'    => $this->tinyInteger(2),
                'size'      => $this->integer()
            ], $tableOptions);

            $this->createTable('{{%contacts_candidate}}', [
                'id'        => $this->primaryKey(),
                'candidate_id'=> $this->char(36)->notNull(),
                'contact_type'=> $this->integer()->notNull(),
                'value'     => $this->string()

            ], $tableOptions);

            $this->createTable('{{%contact_type}}', [
                'id'        => $this->primaryKey(),
                'name'      => $this->string()->notNull(),
            ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%countries}}');
        $this->dropTable('{{%cities}}');
        $this->dropTable('{{%skills}}');
        $this->dropTable('{{%stages}}');
        $this->dropTable('{{%skills_candidate}}');
        $this->dropTable('{{%stages_candidate}}');
        $this->dropTable('{{%notes}}');
        $this->dropTable('{{%documents}}');
        $this->dropTable('{{%contacts}}');
        $this->dropTable('{{%contact_type}}');
    }

}
