<?php

use yii\db\Migration;

/**
 * Class m190402_084809_edit_column_name
 */
class m190402_084809_edit_column_name extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$this->renameColumn('{{%candidate}}', 'futhername', 'fathername');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('{{%candidate}}', 'fathername', 'futhername');
    }
}
