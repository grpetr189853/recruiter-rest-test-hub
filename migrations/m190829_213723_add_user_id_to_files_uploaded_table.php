<?php

use yii\db\Migration;

/**
 * Class m190829_213723_add_user_id_to_files_uploaded_table
 */
class m190829_213723_add_user_id_to_files_uploaded_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%files_uploaded}}', 'user_id', $this->integer(11)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropcolumn('{{%files_uploaded}}', 'user_id');
    }
}
