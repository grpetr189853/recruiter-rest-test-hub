<?php

use yii\db\Migration;

/**
 * Class m190821_115953_file_types
 */
class m190821_115953_file_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%file_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'dir' => $this->string(),
            'table' => $this->string(),

        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('file_type');
    }

}
