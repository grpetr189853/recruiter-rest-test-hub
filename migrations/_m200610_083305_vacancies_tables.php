<?php

use yii\db\Migration;

/**
 * Class m200610_083305_vacancies_tables
 */
class m200610_083305_vacancies_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
        }

        //modify the vacancies table
        $this->addColumn('{{%vacancies}}', 'location', $this->integer());
        $this->addColumn('{{%vacancies}}', 'customer_id', $this->char(36)->notNull());
        $this->addColumn('{{%vacancies}}', 'company_id', $this->char(36)->notNull());
        $this->addColumn('{{%vacancies}}', 'salary', $this->integer());
        $this->addColumn('{{%vacancies}}', 'v_duties', $this->json());
        $this->addColumn('{{%vacancies}}', 'w_conditions', $this->json());
        $this->addColumn('{{%vacancies}}', 'v_requirements', $this->json());
        $this->addColumn('{{%vacancies}}', 'v_description', $this->json());
        
        //create skills_vacancy table
        $this->createTable('{{%skills_vacancy}}', [
            'id'        => $this->primaryKey(),
            'skill_id'  => $this->integer()->notNull(),
            'vacancy_id'=> $this->char(36)->notNull()
        ], $tableOptions);
        //add unique index on skills_vacancy
        $this->execute("ALTER TABLE `skills_vacancy` ADD UNIQUE INDEX `index_vacancy_skills_unique_twice` (`skill_id`, `vacancy_id`) USING BTREE;");
        //change the vacancies id type
        $this->alterColumn('{{%vacancies}}', 'id', $this->char(36)->notNull());
        
        //create vacancy_documents table
        $this->createTable('{{%vacancy_documents}}', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(255)->notNull(),
            'file_extension' => $this->string(10)->notNull(),
            'file_type'=>$this->integer(),
            'file_description'=>$this->string(),
            'file_date' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')),
            'vacancy_id'=> $this->char(36)->notNull(),
            'external_link'=>$this->string(),
        ], $tableOptions);
        //create company table 
        $this->createTable('{{%companies}}', [
            'id' => $this->char(36)->notNull(),
            'name' => $this->string(255)->notNull(),
            'full_name' => $this->string(255)->defaultValue(null),
            'condition' => $this->text()->defaultValue(null),
            'about' => $this->text()->defaultValue(null),
            'logo' => $this->text()->defaultValue(null),
            'image' => $this->text()->defaultValue(null),
        ], $tableOptions);
        $this->addPrimaryKey('pk-companies', '{{%companies}}', 'id');
        //create vacancies skills_vacancy companies and vacancy_documents indexes and foreign keys
        //vacancies
        $this->createIndex(
            'index-vacancies-customer_id',
            '{{%vacancies}}',
            'customer_id');

        $this->addForeignKey(
            'fk-vacancies-customer',
            '{{%vacancies}}',
            'customer_id',
            '{{%customers}}',
            'id',
            'CASCADE',
            'RESTRICT');
        //skills_vacancy
        $this->createIndex(
            'index-vacancy_skills-skill_id',
            '{{%skills_vacancy}}',
            'skill_id');
        $this->addForeignKey(
            'fk-vacancy_skills-skill',
            '{{%skills_vacancy}}',
            'skill_id',
            '{{%skills}}',
            'id',
            'CASCADE',
            'RESTRICT');
        
        $this->createIndex(
            'index-vacancy_skills-vacancy_id',
            '{{%skills_vacancy}}',
            'vacancy_id');

        $this->addForeignKey(
            'fk-vacancy_skills-vacancy',
            '{{%skills_vacancy}}',
            'vacancy_id',
            '{{%vacancies}}',
            'id',
            'CASCADE',
            'RESTRICT');
        //companies
        $this->createIndex(
            'index-vacancies-company_id',
            '{{%vacancies}}',
            'company_id');

        $this->addForeignKey(
            'fk-vacancies-company',
            '{{%vacancies}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'RESTRICT');
        //vacancy_documents
        $this->createIndex(
            'index-vacancy_documents-vacancy_id',
            '{{%vacancy_documents}}',
            'vacancy_id');

        $this->addForeignKey(
            'fk-vacancy_documents-vacancy',
            '{{%vacancy_documents}}',
            'vacancy_id',
            '{{%vacancies}}',
            'id',
            'CASCADE',
            'RESTRICT');
        
        //inserts data in customers table
        $this->batchInsert('{{%customers}}', ['id', 'lastname', 'name', 'fathername', 'location', 'created_at', 'updated_at'], [
            ['9522da5f-713a-4fcd-a9e5-9e56dc03a5a6', 'Customer_LN5', 'Customer_N5', 'Customer_FN5', 7, 1575917401, 1575917401],
            ['9c9b88b5-7a36-4e12-9062-f5d4611d1d77', 'Customer_LN3', 'Customer_N3', 'Customer_FN3', 6, 1575917401, 1575917401],
            ['9f3b5426-df08-483b-8088-02191120c2bf', 'Customer_LN2', 'Customer_N2', 'Customer_FN2', 5, 1575917401, 1575917401],
            ['aee70936-7455-43af-88c1-9bf56393c7d8', 'Customer_LN1', 'Customer_N1', 'Customer_FN1', 1, 1575917401, 1575917401],
            ['d56644ae-a359-4761-aa10-9d8c52d52daa', 'Customer_LN4', 'Customer_N4', 'Customer_FN4', 4, 1575917401, 1575917401],
        ]);
          
          //insert data in companies table
        $this->batchInsert('{{%companies}}',
          ['id','name','full_name','condition','logo'],
          [
              ['52235844-abba-11ea-bb37-0242ac130002','Solring','Solring Holdings Ltd', '5%','solring.png'],
              ['d1e92d1a-abba-11ea-bb37-0242ac130002','Newage','Newage Solutions Ukraine', '7%','newage.png'],
              ['da0fc08a-abba-11ea-bb37-0242ac130002','Codetiburon','Codetiburon', '10%','codetiburon.png'],
              ['e4086fc4-abba-11ea-bb37-0242ac130002','Altium','Altium Limited', '10%','altium.png'],
              ['f07b9772-abba-11ea-bb37-0242ac130002','Ciklum','Ciklum','4%','ciklum.png'],
              ['fa10f21e-abba-11ea-bb37-0242ac130002','SoftServe','CikSoftServe','4%','softserve.png'],
              ['07d32d90-abbb-11ea-bb37-0242ac130002','Innovecs','Innovecs','4%','innovecs.png'],
              ['1327b206-abbb-11ea-bb37-0242ac130002','Luxoft','Luxoft','4%','luxsoft.png'],
              ['1bc29c14-abbb-11ea-bb37-0242ac130002','BETLAB','BETLAB','4%','betlab.png'],
              ['2477a28c-abbb-11ea-bb37-0242ac130002','Levi9','Levi9 Ukraine','4%','levi9.png'],
              ['2cda09b0-abbb-11ea-bb37-0242ac130002','Ring','Ring Ukraine','4%','ring.png'],
          ]
        );          
        //inserts data in vacancies table
        $this->batchInsert('{{%vacancies}}', ['id', 'name', 'created_at', 'updated_at', 'location', 'customer_id', 'company_id', 'salary', 'v_duties', 'w_conditions', 'v_requirements', 'v_description'], [
            ['b95f5d08-ac7f-11ea-bb37-0242ac130002', 'Machine Learning Engineer', 1575917401, 1575917401, 1, '9522da5f-713a-4fcd-a9e5-9e56dc03a5a6', '52235844-abba-11ea-bb37-0242ac130002', 500, 
            $this->lorem(),
            $this->lorem(),
            $this->lorem(),
            $this->lorem()],
            ['db9f362c-ac7f-11ea-bb37-0242ac130002', 'Mobile IOS Developer', 1575917401, 1575917401, 2, '9c9b88b5-7a36-4e12-9062-f5d4611d1d77', 'd1e92d1a-abba-11ea-bb37-0242ac130002', 600, 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem()],
            ['e62f856a-ac7f-11ea-bb37-0242ac130002', 'Mobile Android Developer', 1575917401, 1575917401, 3, '9f3b5426-df08-483b-8088-02191120c2bf', 'da0fc08a-abba-11ea-bb37-0242ac130002', 700, 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem()],
            ['f0bcf616-ac7f-11ea-bb37-0242ac130002', 'FullStack Developer', 1575917401, 1575917401, 4, 'aee70936-7455-43af-88c1-9bf56393c7d8', 'e4086fc4-abba-11ea-bb37-0242ac130002', 800, 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem()],
            ['0c61f470-ac80-11ea-bb37-0242ac130002', 'Embedded Developer', 1575917401, 1575917401, 5, 'd56644ae-a359-4761-aa10-9d8c52d52daa', 'f07b9772-abba-11ea-bb37-0242ac130002', 900, 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem(), 
            $this->lorem()],
          ]);
        // inserts data in skills_vacancy table
        $this->batchInsert('{{%skills_vacancy}}',['id','skill_id','vacancy_id'],[
            [1,1,'b95f5d08-ac7f-11ea-bb37-0242ac130002'],
            [2,2,'b95f5d08-ac7f-11ea-bb37-0242ac130002'],
            [3,3,'db9f362c-ac7f-11ea-bb37-0242ac130002'],
            [4,4,'db9f362c-ac7f-11ea-bb37-0242ac130002'],
            [5,5,'e62f856a-ac7f-11ea-bb37-0242ac130002'],
            [6,6,'e62f856a-ac7f-11ea-bb37-0242ac130002'],
            [7,7,'f0bcf616-ac7f-11ea-bb37-0242ac130002'],
            [8,8,'f0bcf616-ac7f-11ea-bb37-0242ac130002'],
            [9,9,'0c61f470-ac80-11ea-bb37-0242ac130002'],
            [10,10,'0c61f470-ac80-11ea-bb37-0242ac130002'],
        ]);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //delete all data in skills_vacancy table
        \Yii::$app->db->createCommand()->delete('{{%skills_vacancy}}',['in','vacancy_id',[
            'b95f5d08-ac7f-11ea-bb37-0242ac130002',
            'db9f362c-ac7f-11ea-bb37-0242ac130002',
            'e62f856a-ac7f-11ea-bb37-0242ac130002',
            'f0bcf616-ac7f-11ea-bb37-0242ac130002',
            '0c61f470-ac80-11ea-bb37-0242ac130002',]
        ])->execute();
        //delete all data in companies table
        \Yii::$app->db->createCommand()->delete('{{%companies}}', ['in', 'id', [
            '52235844-abba-11ea-bb37-0242ac130002',
            'd1e92d1a-abba-11ea-bb37-0242ac130002', 
            'da0fc08a-abba-11ea-bb37-0242ac130002', 
            'e4086fc4-abba-11ea-bb37-0242ac130002', 
            'f07b9772-abba-11ea-bb37-0242ac130002',
            'fa10f21e-abba-11ea-bb37-0242ac130002',
            '07d32d90-abbb-11ea-bb37-0242ac130002',
            '1327b206-abbb-11ea-bb37-0242ac130002',
            '1bc29c14-abbb-11ea-bb37-0242ac130002',
            '2477a28c-abbb-11ea-bb37-0242ac130002',
            '2cda09b0-abbb-11ea-bb37-0242ac130002']]
        )->execute();
        
        //delete all data in vacancies table
        \Yii::$app->db->createCommand()->delete('{{%vacancies}}', ['in', 'id', [
            'b95f5d08-ac7f-11ea-bb37-0242ac130002',
            'db9f362c-ac7f-11ea-bb37-0242ac130002', 
            'e62f856a-ac7f-11ea-bb37-0242ac130002', 
            'f0bcf616-ac7f-11ea-bb37-0242ac130002', 
            '0c61f470-ac80-11ea-bb37-0242ac130002'
            ]]
        )->execute();
        //delete all data in customers table
        \Yii::$app->db->createCommand()->delete('{{%customers}}', ['in', 'id', [
            '9522da5f-713a-4fcd-a9e5-9e56dc03a5a6',
            '9c9b88b5-7a36-4e12-9062-f5d4611d1d77', 
            '9f3b5426-df08-483b-8088-02191120c2bf', 
            'aee70936-7455-43af-88c1-9bf56393c7d8', 
            'd56644ae-a359-4761-aa10-9d8c52d52daa']]
        )->execute();

        $this->dropForeignKey('fk-vacancies-customer','{{%vacancies}}');
        $this->dropIndex('index-vacancies-customer_id','{{%vacancies}}');

        $this->dropForeignKey('fk-vacancies-company','{{%vacancies}}');
        $this->dropIndex('index-vacancies-company_id','{{%vacancies}}');

        $this->dropColumn('{{%vacancies}}', 'location');
        $this->dropColumn('{{%vacancies}}', 'customer_id');
        $this->dropColumn('{{%vacancies}}', 'company_id');
        $this->dropColumn('{{%vacancies}}', 'salary');
        $this->dropColumn('{{%vacancies}}', 'v_duties');
        $this->dropColumn('{{%vacancies}}', 'w_conditions');
        $this->dropColumn('{{%vacancies}}', 'v_requirements');
        $this->dropColumn('{{%vacancies}}', 'v_description');

        $this->dropForeignKey('fk-vacancy_skills-skill','{{%skills_vacancy}}');
        $this->dropIndex('index-vacancy_skills-skill_id','{{%skills_vacancy}}');
        $this->dropForeignKey('fk-vacancy_skills-vacancy','{{%skills_vacancy}}');
        $this->dropIndex('index-vacancy_skills-vacancy_id','{{%skills_vacancy}}');

        $this->dropForeignKey('fk-vacancy_documents-vacancy','{{%vacancy_documents}}');
        $this->dropIndex('index-vacancy_documents-vacancy_id','{{%vacancy_documents}}');


        $this->dropTable('{{%skills_vacancy}}');

        $this->dropTable('{{%vacancy_documents}}');

        $this->dropTable('{{%companies}}');

        // $this->alterColumn('{{%vacancies}}', 'id', $this->primaryKey());
        
        
    }

    public function lorem() {
        return '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>';
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200610_083305_vacancies_tables cannot be reverted.\n";

        return false;
    }
    */
}
