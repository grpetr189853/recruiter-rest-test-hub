<?php

use yii\db\Migration;

/**
 * Class m190821_115741_table_files_uploaded
 */
class m190821_115741_table_files_uploaded extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%files_uploaded}}', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string(255)->notNull(),
            'file_extension' => $this->string(10)->notNull(),
            'file_type'=>$this->integer(),
            'file_description'=>$this->string(),
            'file_date' => $this->timestamp()->defaultValue(new \yii\db\Expression('NOW()')),
            'entity_id'=> $this->string(),
            'external_link'=>$this->string(),

        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('files_uploaded');
    }

}
