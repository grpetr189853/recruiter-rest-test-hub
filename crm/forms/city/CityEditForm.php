<?php
namespace app\crm\forms\city;

use app\crm\interfaces\ICity;
use yii\base\Model;

class CityEditForm extends Model
{
    public $id;
    public $name;
    public $country_id;

    private $city;

    public function __construct(ICity $city,$config = [])
    {
        $this->city = $city;
        $this->id = $city->getId();
        $this->name = $city->getName();
        $this->country_id = $city->getCountryId();
        parent::__construct($config);
    }
    public function rules()
    {
        return [
            [[ 'name', ], 'string'],
            [['id','country_id'], 'integer'],
            [['name', 'country_id'], 'required'],
        ];
    }
}