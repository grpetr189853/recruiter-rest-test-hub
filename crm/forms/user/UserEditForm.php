<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 15.04.2019
 * Time: 10:10
 */
namespace app\crm\forms\user;


use \common\models\User;
use app\crm\interfaces\IUser;
use yii\base\Model;


class UserEditForm extends Model
{
    public $id;
    public $name;
    public $lastname;
    public $date_birth;
    public $location_id;
    public $email;
    public $group;
    public $old_password;
    public $password;
    public $repeat_password;
    public $status;

    private $user;

    /**
     *
     */
    const SCENARIO_PASSWORD = 'password';
    /**
     *
     */
    const SCENARIO_DEFAULT = 'default';

    public function __construct(IUser $user, array $config = [])
    {
        $this->user = $user;
        $this->id = $user->getId()->getId();
        $this->name = $user->getName()->getFirst();
        $this->lastname = $user->getName()->getLast();
        $this->date_birth =  $user->getDateBirth();
        $this->location_id = $user->getLocation();
        $this->email = $user->getEmail();
        $this->group = $user->getGroupName();
        $this->old_password = null;
        $this->password = $user->getPassword();
        $this->repeat_password = $user->getRepeatPassword();
        $this->status = $user->getStatus();
        parent::__construct($config);
    }


    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_PASSWORD => ['name', 'lastname', 'date_birth', 'location_id', 'email', 'group', 'old_password', 'password', 'repeat_password'],
            self::SCENARIO_DEFAULT => ['name', 'lastname', 'date_birth', 'location_id', 'email', 'group', 'old_password', 'password', 'repeat_password'],
        ];
    }

    public function rules(): array
    {
        return [
            [['id', 'name', 'lastname', 'date_birth', 'email', 'group', 'old_password', 'password', 'repeat_password'], 'string'],
            [['location_id'], 'integer'],
            [['name', 'lastname', 'email', 'group'], 'required'],
            ['email', 'email'],
            ['email', 'checkEmail', 'params' => ['message' => 'Email exist']],
            ['group', 'checkGroup', 'params' => ['message' => 'Group exist']],
            [['old_password', 'password', 'repeat_password'], 'safe'],
            ['old_password', 'checkPassword', 'params' => ['message' => 'Password incorrect'], 'on' => self::SCENARIO_PASSWORD],
            [['old_password', 'password', 'repeat_password',], 'required', 'on' => self::SCENARIO_PASSWORD],
            [['old_password', 'password'], 'string', 'min' => 6, 'max' => 40, 'on' => self::SCENARIO_PASSWORD],
            ['repeat_password', 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_PASSWORD],
            ['status', 'default', 'value' => User::STATUS_ACTIVE],
            ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_DELETED]],
        ];
    }

    public function checkEmail($attribute,$params)
    {
        switch($attribute){
            case "email":
                if($this->user->getEmail() != $this->email) {
                    $model = User::findByUsername($this->user->getUsername());
                    if($model){
                        $this->addError($attribute, $params['message']);
                    }
                }
                break;
        }
    }

    public function checkGroup($attribute, $params)
    {
        switch($attribute){
            case "group":
                if($this->user->getGroupName() != $this->group) {}
                break;
        }
    }

    public function checkPassword($attribute, $params)
    {
        switch($attribute){
            case "old_password":
                $model = User::findByUsername($this->user->getUsername());
                if(empty($model) || !$model->validatePassword($this->old_password)) {
                    $this->addError($attribute, $params['message']);
                }
                break;
        }
    }
}