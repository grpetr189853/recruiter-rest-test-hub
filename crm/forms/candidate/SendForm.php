<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 15.04.2019
 * Time: 10:10
 */

namespace app\crm\forms\candidate;


use app\crm\interfaces\ICandidate;
use yii\base\Model;
use yii\helpers\ArrayHelper;


class SendForm extends Model
{
    public $customer;
    public $vacancy;
    public $contact;
    public $document;
    public $address;
    public $letter;
    public $approved;

    public $customers = [];
    public $vacancies = [];
    public $contacts = [];
    public $addresses = [];
    public $documents = [];

    const SCENARIO_CUSTOMER = 'customer';
    const SCENARIO_CANDIDATE = 'candidate';

    public function __construct(ICandidate $candidate, array $config = [])
    {
        $this->customer = null;
        $this->vacancy = null;
        $this->contact = null;
        $this->document = null;
        $this->address = null;
        $this->letter = null;
        $this->approved = true;

        $this->customers = [];
        $this->vacancies = [];
        $this->contacts = [];
        $this->addresses = [];
        $this->documents = [];

        parent::__construct($config);
    }


    public function rules(): array
    {
        return [
            [['customer', 'address', 'letter', 'document'], 'string'],
            [['vacancy', 'contact'], 'integer'],
            [['customer', 'vacancy', 'contact', 'address'], 'required', 'on' => self::SCENARIO_CUSTOMER],
            [['contact', 'address'], 'required', 'on' => self::SCENARIO_CANDIDATE],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CUSTOMER => ['customer', 'vacancy', 'contact', 'address', 'letter', 'document'],
            self::SCENARIO_CANDIDATE => ['contact', 'address', 'letter', 'document'],
        ];
    }
}