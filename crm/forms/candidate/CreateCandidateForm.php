<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 25.04.2019
 * Time: 16:46
 */

namespace app\crm\forms\candidate;


class CreateCandidateForm
{
    public $lastname;
    public $name;
    public $fathername;
    public $salary;
    public $location;
    public $current_company;
    public $current_position;
    public $birth;
    public $photo;
    public $wish;
    public $contacts=[];
    public $skills=[];
    public $documents=[];
}