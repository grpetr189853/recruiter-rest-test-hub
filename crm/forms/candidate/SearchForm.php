<?php

namespace app\crm\forms\candidate;


use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\crm\interfaces\ICandidate;

class SearchForm extends Model
{
    public $lastname;
    public $name;
    public $salary;
    public $location;
    public $skills=[];

    public function rules(): array
    {
        return [
            [['name','lastname'], 'string'],
            [['salary','location'], 'integer'],
            [['skills','contacts'],'safe'],
        ];
    }
}