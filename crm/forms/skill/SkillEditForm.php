<?php
namespace app\crm\forms\skill;

use app\crm\interfaces\ISkill;
use yii\base\Model;

class SkillEditForm extends Model
{
    public $id;
    public $name;
    private $skill;

    public function __construct(ISkill $skill,$config = [])
    {
        $this->skill = $skill;
        $this->id = $skill->getId();
        $this->name = $skill->getName();
        parent::__construct($config);
    }
    public function rules()
    {
        return [
            [[ 'name', ], 'string'],
            [['id'], 'integer'],
            [['name'], 'required'],
        ];
    }
}