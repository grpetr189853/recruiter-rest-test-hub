<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 03.04.2019
 * Time: 15:33
 */

namespace app\crm\entities\candidate;


use app\crm\entities\_traits\InstantiateTrait;
use app\crm\entities\Skill;
use app\crm\interfaces\IARCandidateRelationThing;
use app\crm\interfaces\ISkill;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Assert\Assertion;

/***
 * Class SkillCandidate
 * @package app\crm\entities\candidate
 *
 * @property Skill $skill
 *
 */
class SkillCandidate extends ActiveRecord implements ISkill, IARCandidateRelationThing
{
    use InstantiateTrait;

    private $skill_id;
    private $candidate_id;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
/*
    public function __construct(int $skill_id)
    {
        $this->skill_id = $skill_id;

        parent::__construct();
    }
*/

    public static function create(int $skill_id,string $candidate_id): self
    {
        $skill_candidate = new static();
        $skill_candidate->skill_id = $skill_id;
        $skill_candidate->candidate_id = $candidate_id;
        return $skill_candidate;
    }

    public function getSkillId()
    {
        return $this->skill_id;
    }

    public function getCandidateId()
    {
        return $this->candidate_id;
    }


    public function isEqualTo(ISkill $skill): bool
    {
        return $this->skill_id === $skill->skill_id;
    }

    /***### - ###***/
    public static function tableName(): string
    {
        return '{{%skills_candidate}}';
    }

    public function afterFind(): void
    {
        $this->skill_id = $this->getAttribute('skill_id');
        $this->candidate_id = $this->getAttribute('candidate_id');
        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {

        return parent::beforeSave($insert);
    }

    public function getSkill(): ActiveQuery
    {
        return $this->hasOne(Skill::className(), ['id'=>'skill_id']);
    }

    public function rules()
    {
        return [
            [['skill_id','candidate_id'], 'safe','on' => self::SCENARIO_CREATE],
            [['skill_id','candidate_id'], 'safe','on' => self::SCENARIO_UPDATE],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['skill_id','candidate_id'],
            self::SCENARIO_UPDATE => ['skill_id','candidate_id'],
        ];
    }


}
