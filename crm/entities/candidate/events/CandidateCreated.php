<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 15:09
 */

namespace app\crm\entities\candidate\events;


use app\crm\entities\candidate\CandidateId;

class CandidateCreated
{
    public $candidateId;

    public function __construct(CandidateId $id)
    {
        $this->candidateId = $id;
    }
}