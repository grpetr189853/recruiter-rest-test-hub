<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 14:35
 */

namespace app\crm\entities\candidate\events;


use app\crm\entities\candidate\CandidateId;
use app\crm\entities\candidate\Name;

class CandidateRenamed
{
    public $candidateId;
    public $name;

    public function __construct(CandidateId $candidateId, Name $name)
    {
        $this->candidateId = $candidateId;
        $this->name = $name;
    }
}