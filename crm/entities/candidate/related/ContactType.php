<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 11.04.2019
 * Time: 14:30
 */

namespace app\crm\entities\candidate\related;

use yii\db\ActiveRecord;

class ContactType extends ActiveRecord
{


    public static function tableName(): string
    {
        return '{{%contact_type}}';
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}