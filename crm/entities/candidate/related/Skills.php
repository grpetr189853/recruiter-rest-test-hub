<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 03.04.2019
 * Time: 17:10
 */

namespace app\crm\entities\candidate\related;


use app\crm\entities\candidate\SkillCandidate;
use app\crm\interfaces\ISkill;

class Skills
{

    private $skills = [];

    public function __construct(array $skills)
    {
        //var_dump($skills);die;
        if(!$skills){
            //throw new \DomainException('May be give me one skill?');
        }
        foreach ($skills as $contact){
            $this->add($contact);
        }
    }

    public function add(ISkill $skill): void
    {
        //var_dump(($skill instanceof ISkill));die;

        foreach ($this->skills as $item) {
            if($item->isEqualTo($skill)) {
                throw new \DomainException('Skill already exists.');
            }
        }

        $this->skills[] = $skill;

    }

    public function remove($index): ISkill
    {
        if(!isset($this->skills[$index])){
            throw new \DomainException('Skill not found.');
        }
        $skill = $this->skills[$index];
        unset($this->skills[$index]);

        return $skill;
    }

    public function getAll(): array
    {
        return $this->skills;
    }
}