<?php
/**
 * Created by PhpStorm.
 * User: grpetr189853
 * Date: 28.09.2019
 * Time: 15:53
 */

namespace app\crm\entities\candidate;


class Documents
{
    /***@var \app\crm\entities\candidate\Documents[] ***/
    private $documents = [];

    public function __construct(array $documents)
    {
        if(!$documents){
            //throw new \DomainException('May be give me one document?');
        }
        foreach ($documents as $document){
            $this->add($document);
        }
    }
    public function add($document): void
    {
        foreach ($this->documents as $item) {
            if(false) { //TODO:
                throw new \DomainException('Document already exists.');
            }
        }
        $this->documents[] = $document;
    }

    public function remove($index)
    {
        if(!isset($this->documents[$index])){
            throw new \DomainException('Contact not found.');
        }
        $document = $this->documents[$index];
        unset($this->documents[$index]);

        return $document;
    }

    public function getAll(): array
    {
        return $this->documents;
    }
}