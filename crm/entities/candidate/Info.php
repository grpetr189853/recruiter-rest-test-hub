<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 02.04.2019
 * Time: 11:56
 */

namespace app\crm\entities\candidate;

use Assert\Assertion;

class Info
{
    private $salary;
    private $company;
    private $position;
    private $birth;

    public function __construct(int $salary = null, string $company = null, string $position = null, \DateTimeImmutable $birth)
    {
        $this->salary = $salary;
        $this->company = $company;
        $this->position= $position;
        $this->birth = $birth;
    }

    public function getCurrentCompany(): string
    {
        return $this->company;
    }

    public function getCurrentWork(): string
    {
        return $this->position . ' (' . $this->company .') ';
    }

    public function getCurrentPosition(): string
    {
        return $this->position;
    }

    public function getSalary(): int
    {
        return $this->salary;
    }

    public function getBirth()
    {
        return $this->birth;
    }




}