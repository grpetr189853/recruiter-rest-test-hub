<?php
/**
 * Created by PhpStorm.
 * User: grpetr189853
 * Date: 28.09.2019
 * Time: 15:54
 */

namespace app\crm\entities\candidate;

use app\crm\entities\_traits\InstantiateTrait;
use app\crm\interfaces\IARCandidateRelationThing;
use app\crm\interfaces\IDocument;
use app\crm\interfaces\IEntityId;
use yii\db\ActiveRecord;
use Assert\Assertion;

class DocumentCandidate extends ActiveRecord implements IDocument,IARCandidateRelationThing
{
    use InstantiateTrait;

    private $file_name;
    private $file_extension;
    private $entity_id;
    private $is_resume;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_DELETE = 'delete';
    const IS_DOCUMENT = 0;
    const IS_RESUME = 1;
    /*
    public function __construct(string $fileName,string $fileExtension,string $entityId)
    {
        Assertion::notEmpty($fileName);
        Assertion::notEmpty($fileExtension);
        Assertion::notEmpty($entityId);

        $this->fileName = $fileName;
        $this->fileExtension = $fileExtension;
        $this->entityId = $entityId;

        parent::__construct();
    }
    */

    public function create(string $fileName,string $fileExtension,string $entityId,int $is_resume): self
    {
        $document_candidate = new static();
        $document_candidate->file_name = $fileName;
        $document_candidate->file_extension = $fileExtension;
        $document_candidate->entity_id = $entityId;
        $document_candidate->is_resume = $is_resume;
        return $document_candidate;
    }

    public function getFileName(): string
    {
        return $this->file_name;
    }

    public function getFileExtension(): string
    {
        return $this->file_extension;
    }

    public function getEntityId(): string
    {
        return $this->entity_id;
    }

    public function getIsResume() : int
    {
        return $this->is_resume;
    }

    public static function tableName(): string
    {
        return '{{%files_uploaded}}';
    }

    public function afterFind(): void
    {
        $this->file_name = $this->getAttribute('file_name', $this->file_name);
        $this->file_extension = $this->getAttribute('file_extension', $this->file_extension);
        $this->entity_id = $this->getAttribute('entity_id', $this->entity_id);
        $this->is_resume = $this->getAttribute('is_resume', $this->is_resume);

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
//        $this->setAttribute('file_name', $this->fileName);
//        $this->setAttribute('file_extension', $this->fileExtension);
//        $this->setAttribute('entity_id', $this->entityId);
        return parent::beforeSave($insert);
    }

    public function rules()
    {
        return [
            [['file_name','file_extension','entity_id','is_resume'], 'safe','on' => self::SCENARIO_CREATE],
            [['file_name','file_extension','entity_id','is_resume'], 'safe','on' => self::SCENARIO_UPDATE],
            [['file_name','file_extension','entity_id','is_resume'], 'safe','on' => self::SCENARIO_DELETE],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['file_name','file_extension','entity_id','is_resume'],
            self::SCENARIO_UPDATE => ['file_name','file_extension','entity_id','is_resume'],
            self::SCENARIO_DELETE => ['file_name','file_extension','entity_id','is_resume'],
        ];
    }
}