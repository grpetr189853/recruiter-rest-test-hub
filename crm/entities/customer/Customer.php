<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 28.03.2019
 * Time: 15:31
 */

namespace app\crm\entities\customer;

use app\crm\entities\_traits\LazyLoadTrait;
use app\crm\entities\candidate\Contacts;
use app\crm\entities\customer\CustomerId;
use app\crm\entities\candidate\Name;
use app\crm\entities\City;
use app\crm\interfaces\IARCustomer;
use app\crm\interfaces\ICustomer;
use app\crm\interfaces\IEntity;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use ProxyManager\Proxy\LazyLoadingInterface;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/***
 * Class Customer
 * @package app\crm\entities\customer
 *
 * @property ContactCustomer[] $relatedContacts
 * @property City $city
 */
class Customer extends ActiveRecord implements IEntity, ICustomer, IARCustomer
{
    use LazyLoadTrait;

    /***@var $id CustomerId***/
    private $id;

    /***@var $name Name***/
    private $name;

    /***@var $location Location***/
    private $location;
    
    /***@var Contacts***/
    private $contacts;
    
    public static function create(CustomerId $id, Name $name, int $location_id = null, array $contacts = []): self
    {
        $customer = new static();
        $customer->id              = $id;
        $customer->name            = $name;
        $customer->location        = $location_id;
        $customer->contacts        = new Contacts($contacts);

        return $customer;
    }

    public function rename(Name $name): void
    {
        $this->name = $name;
    }

    public function remove(): void
    {
        if(false){
            throw new \DomainException('Some reason why can not delete customer');
        }
    }

    public function getId(): CustomerId
    {
        return $this->id;
    }

    public function getName(): Name
    {
        return $this->name;
    }
    
    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function setCurrentContacts(Contacts $contacts):void
    {
        $this->contacts = $contacts;
    }

    public function getCurrentContacts(): Contacts
    {
        return $this->contacts;
    }

    public function getContacts(): array
    {
        return $this->contacts->getAll();
    }

    private function prepareContacts(): array
    {
        $data = [];
        foreach ($this->getContacts() as $key=>$value) {
            $data[$value->type->name.$key] = $value->value;
        }
        return $data;
    }

    public function export(): array
    {
        $data = [
            'location'  => $this->city->name??null,

        ];
        $contacts = $this->prepareContacts();
        $data = array_merge($data,$contacts);
        return $data;
    }

    public function getType(): string
    {
        return "customer";
    }

    /**
     * @return array
     */
    public function releaseEvents(): array
    {
        // TODO: Implement releaseEvents() method.
        return [];
    }


    /***##SERVICE METHODS **/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customers}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' =>  SaveRelationsBehavior::className(),
                'relations' => ['relatedContacts'],
            ],
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function afterFind(): void
    {
        $this->id = new CustomerId(
            $this->getAttribute('id')
        );

        $this->name = new Name(
            $this->getAttribute('lastname'),
            $this->getAttribute('name'),
            $this->getAttribute('fathername')
        );

        $this->contacts = self::getLazyFactory()->createProxy(
            Contacts::class,
            function(&$target, LazyLoadingInterface $proxy) {
                $target = new Contacts($this->relatedContacts);
                $proxy->setProxyInitializer(null);
            }
        );

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        $this->setAttribute('id',           $this->id->getId());
        $this->setAttribute('lastname',     $this->name->getLast());
        $this->setAttribute('name',         $this->name->getFirst());
        $this->setAttribute('fathername',   $this->name->getFather());

        if (!$this->contacts instanceOf LazyLoadingInterface || $this->contacts->isProxyInitialized()) {
            $this->relatedContacts = $this->contacts->getAll();
        }

        return parent::beforeSave($insert);
    }

    /***RELATIONS***/

    public function getRelatedContacts(): ActiveQuery
    {
        return $this->hasMany(ContactCustomer::className(), ['customer_id' => 'id'])->orderBy('contact_type');
    }

    public function getCity()
    {
        return $this->hasOne(City::className(),['id'=>'location']);
    }

}