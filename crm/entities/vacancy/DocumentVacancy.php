<?php
/**
 * Created by PhpStorm.
 * User: grpetr189853
 * Date: 28.09.2019
 * Time: 15:54
 */

namespace app\crm\entities\vacancy;

use app\crm\entities\_traits\InstantiateTrait;
use app\crm\interfaces\IARCandidateRelationThing;
use app\crm\interfaces\IDocument;
use app\crm\interfaces\IEntityId;
use yii\db\ActiveRecord;
use Assert\Assertion;

class DocumentVacancy extends ActiveRecord implements IDocument, IARCandidateRelationThing
{
    use InstantiateTrait;

    private $file_name;
    private $file_extension;
    private $vacancy_id;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_DELETE = 'delete';

    public function create(string $fileName,string $fileExtension,string $vacancyId): self
    {
        $document_vacancy = new static();
        $document_vacancy->file_name = $fileName;
        $document_vacancy->file_extension = $fileExtension;
        $document_vacancy->vacancy_id = $vacancyId;
        return $document_vacancy;
    }

    public function getFileName(): string
    {
        return $this->file_name;
    }

    public function getFileExtension(): string
    {
        return $this->file_extension;
    }

    public static function tableName(): string
    {
        return '{{%vacancy_documents}}';
    }

    public function afterFind(): void 
    {
        $this->file_name = $this->getAttribute('file_name', $this->file_name);
        $this->file_extension = $this->getAttribute('file_extension', $this->file_extension);
        $this->vacancy_id = $this->getAttribute('vacancy_id', $this->vacancy_id);

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {

        return parent::beforeSave($insert);
    }

    public function rules()
    {
        return [
            [['file_name','file_extension','vacancy_id'], 'safe','on' => self::SCENARIO_CREATE],
            [['file_name','file_extension','vacancy_id'], 'safe','on' => self::SCENARIO_UPDATE],
            [['file_name','file_extension','vacancy_id'], 'safe','on' => self::SCENARIO_DELETE],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['file_name','file_extension','vacancy_id'],
            self::SCENARIO_UPDATE => ['file_name','file_extension','vacancy_id'],
            self::SCENARIO_DELETE => ['file_name','file_extension','vacancy_id'],
        ];
    }
}
