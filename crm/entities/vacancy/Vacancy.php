<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 28.03.2019
 * Time: 15:31
 */

namespace app\crm\entities\vacancy;

use app\crm\entities\_traits\LazyLoadTrait;
use app\crm\entities\candidate\ContactCandidate;
use app\crm\entities\candidate\DocumentCandidate;
use app\crm\entities\candidate\Documents;
use app\crm\entities\candidate\related\Skills;
use app\crm\entities\candidate\SkillCandidate;
use app\crm\entities\City;
use app\crm\entities\company\Company;
use app\crm\entities\customer\Customer;
use app\crm\entities\vacancy\VacancyId;
use app\crm\interfaces\IARVacancy;
use app\crm\interfaces\IVacancy;
use app\crm\interfaces\IEntity;
use ProxyManager\Proxy\LazyLoadingInterface;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
/***
 * Class Vacancy
 * @package app\crm\entities\vacancy
 *
 * @property Company[] $relatedCompanies
 * @property Customer[] $relatedCustomers
 * @property SkillVacancy[] $relatedSkills
 * @property City $city
 */
class Vacancy extends ActiveRecord implements IEntity, IVacancy, IARVacancy
{
    use LazyLoadTrait;

    /***@var $id VacancyId***/
    private $id;

    /***@var $name String***/
    private $name;

    /***@var Company[] ***/
    private $company_id;

    private $customer_id;

    private $location;

    /***@var Skills***/
    private $skills;

    private $salary;

    private $v_duties;

    private $w_conditions;

    private $v_requirements;

    private $v_description;

    private $documents;

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_CREATE = 'create';

    public static function create(VacancyId $id, string $name, string $location, int $salary,string $customer_id,string $company, array $skills=[], string $v_duties, string $w_conditions, string $v_requirements, string $v_description, array $documents = []): self
    {
        $vacancy = new static();
        $vacancy->id   = $id;
        $vacancy->name = $name;
        $vacancy->location = $location;
        $vacancy->salary = $salary;
        $vacancy->customer_id = $customer_id;
        $vacancy->company_id = $company;
        $vacancy->skills = new Skills($skills);
        $vacancy->v_duties = $v_duties;
        $vacancy->w_conditions = $w_conditions;
        $vacancy->v_requirements = $v_requirements;
        $vacancy->v_description = $v_description;
//        $vacancy->documents = new Documents($documents);
        return $vacancy;
    }

    public function rename(string $name): void
    {
        $this->name = $name;
    }

    public function remove(): void
    {
        if(false){
            throw new \DomainException('Some reason why can not delete vacancy');
        }
    }

    public function getId(): VacancyId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getSalary() 
    {
        return $this->salary;
    }

    public function getCompanyId()
    {
        return $this->company_id;
    }

    public function getCustomerId()
    {
        return $this->customer_id;
    }

    public function getVDuties()
    {
        return $this->v_duties;
    }

    public function getWConditions()
    {
        return $this->w_conditions;
    }

    public function getVRequirements() 
    {
        return $this->v_requirements;
    }

    public function getVDescription() 
    {
        return $this->v_description;
    }

    public function export(): array
    {
        $data = [];
        return $data;
    }

    public function getType(): string
    {
        return "vacancy";
    }

    /**
     * @return array
     */
    public function releaseEvents(): array
    {
        // TODO: Implement releaseEvents() method.
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancies}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' =>  SaveRelationsBehavior::className(),
                'relations' => ['relatedCustomers','relatedCompanies','relatedSkills','relatedDocuments'],
            ],
        ];
    }

    public function extraFields()
    {
        return [
            'relatedCompanies' => 'relatedCompanies', 'city' => 'city', 'relatedCustomers' => 'relatedCustomers', 'relatedSkills' => 'relatedSkills'
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_UPDATE => ['id','name','location','salary','company_id','customer_id','skills','v_duties','w_conditions','v_requirements','v_description'],
            self::SCENARIO_DEFAULT => ['id','name','location','salary','company_id','customer_id','skills','v_duties','w_conditions','v_requirements','v_description'],
            self::SCENARIO_CREATE => ['id','name','location','salary','company_id','customer_id','skills','v_duties','w_conditions','v_requirements','v_description'],
        ];
    }

    public function rules()
    {
        return [
            [['id','name','location','salary','company_id','customer_id','skills','v_duties','w_conditions','v_requirements','v_description'], 'safe','on' => self::SCENARIO_UPDATE],
            [['id','name','location','salary','company_id','customer_id','skills','v_duties','w_conditions','v_requirements','v_description'], 'safe','on' => self::SCENARIO_DEFAULT],
            [['id','name','location','salary','company_id','customer_id','skills','v_duties','w_conditions','v_requirements','v_description'], 'safe','on' => self::SCENARIO_CREATE],
        ];
    }

    public function afterFind(): void
    {
        $this->id = new VacancyId(
            $this->getAttribute('id')
        );

        $this->name = $this->getAttribute('name');

        $this->location = $this->getAttribute('location');

        $this->company_id = self::getLazyFactory()->createProxy(
            Company::class,
            function(&$target, LazyLoadingInterface $proxy) {
                $target = new Company($this->relatedCompanies);
                $proxy->setProxyInitializer(null);
            }
        );

        $this->customer_id = self::getLazyFactory()->createProxy(
            Customer::class,
            function(&$target, LazyLoadingInterface $proxy) {
                $target = new Customer($this->relatedCustomers);
                $proxy->setProxyInitializer(null);
            }
        );

        $this->skills = self::getLazyFactory()->createProxy(
            Skills::class,
            function(&$target, LazyLoadingInterface $proxy) {
                $target = new Skills($this->relatedSkills);
                $proxy->setProxyInitializer(null);
            }
        );

        parent::afterFind();
    }

    public function beforeSave($insert): bool
    {
        $this->setAttribute('id', $this->id->getId());
        $this->setAttribute('name', $this->getName());
        if($insert) {
            $this->setAttribute('created_at', (new \DateTime())->getTimestamp());
            $this->setAttribute('updated_at', (new \DateTime())->getTimestamp());
            $this->setAttribute('location', $this->getLocation());
            $this->setAttribute('salary', $this->getSalary());
            $this->setAttribute('company_id',$this->getCompanyId());
            $this->setAttribute('customer_id',$this->getCustomerId());
            $this->setAttribute('v_duties', $this->getVDuties());
            $this->setAttribute('w_conditions', $this->getWConditions());
            $this->setAttribute('v_requirements', $this->getVRequirements());
            $this->setAttribute('v_description', $this->getVDescription());
        }

        /*
        if (!$this->company_id instanceOf LazyLoadingInterface || $this->company_id->isProxyInitialized()) {
            $this->relatedCompanies = $this->company_id->getAll();
        }

        if (!$this->customers instanceOf LazyLoadingInterface || $this->customers->isProxyInitialized()) {
            $this->relatedCustomers = $this->customers->getAll();
        }
        */
        if (!$this->skills instanceOf LazyLoadingInterface || $this->skills->isProxyInitialized()) {
            $this->relatedSkills = $this->skills->getAll();
        }

        return parent::beforeSave($insert);
    }

    public function getRelatedCustomers() 
    {
        return $this->hasMany(Customer::className(),['id' => 'customer_id'])->orderBy('id');
    }

    public function getRelatedCompanies() 
    {
        return $this->hasMany(Company::className(), ['id'=>'company_id'])->orderBy('id');

    }

    public function getRelatedSkills(): ActiveQuery
    {
        return $this->hasMany(SkillVacancy::className(), ['vacancy_id' => 'id'])->orderBy('skill_id');
    }

    public function getCity() 
    {
        return $this->hasOne(City::className(),['id'=>'location']);
    }
}