<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 28.03.2019
 * Time: 23:56
 */

namespace app\crm\tests\unit\repoitories;

use Assert\InvalidArgumentException;
use Codeception\Test\Unit;
use app\crm\entities\candidate\Candidate;
use app\crm\entities\candidate\CandidateId;
use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\repositories\candidates\ARCandidateRepository;
use app\crm\repositories\NotFoundException;
use yii\db\Exception;
use app\crm\entities\candidate\ContactCandidate;

class CandidateRepoTest extends Unit
{
    public function testNext()
    {
        $repo = new ARCandidateRepository();
        $this->assertEquals(36, strlen($repo->nextId()->getId()));
        codecept_debug($repo->nextId()->getId());
    }

    public function testAdd()
    {
        /***@var $repo ARCandidateRepository***/
        $repo = new ARCandidateRepository();

        $id = new CandidateId($repo->nextId()->getId());
        $name = new Name('Dmytro','Mytsko', "Ivanovich");
        $info = new Info(3500, 'Sunk Int.', 'Senior Pascal Developer', new \DateTimeImmutable('2000-04-28'));
        $email = new ContactCandidate(2,'my@gmail.com');
        $skype = new ContactCandidate(3,'jaskins.gaskins');
        $phone = new ContactCandidate(1,'+3805000000000');

        $candidate = new Candidate($id, $name, $info,[$phone, $email, $skype]);
        $repo->add($candidate);

        /*try {
            $repo->add($candidate);
        } catch (\RuntimeException $e){
            $this->assertEquals('Inserting error', $e->getMessage());
        }*/
    }

    public function testUpdate()
    {
        /***@var $repo ARCandidateRepository***/
        $repo = new ARCandidateRepository();

        /***@var $candidate Candidate***/
        $candidate = $repo->getBy([]);

        $name = new Name('Dmytro-'.time(),'Mytsko-'.time(), "Ivanovich-".time());
        $candidate->rename($name);
        $repo->save($candidate);
        


        /*try {
            $repo->save($candidate);
        }
        catch (\RuntimeException $e){
            $this->assertNotEquals('Saving error', $e->getMessage());
        }*/
    }




    public function testGet()
    {
        /***@var $repo ARCandidateRepository***/
        $repo = new ARCandidateRepository();
        $candidate = $repo->getBy([]);

        $this->assertInstanceOf(Candidate::class,$candidate);
        codecept_debug($candidate->getAttributes());
        codecept_debug($candidate->getContacts());
    }

    public function testGetFail()
    {
        /***@var $repo ARCandidateRepository***/
        $repo = new ARCandidateRepository();
        try {
            $repo->getBy(['ids'=>'not_exist_id']);
        }
        catch (NotFoundException $e) {
            $this->assertEquals('Candidate not found', $e->getMessage());
        }
        catch (Exception $e){
            $this->assertEquals('42', $e->getCode());
        }
    }

    public function testGetAll()
    {
        /***@var $repo ARCandidateRepository***/
        $repo = new ARCandidateRepository();
        $candidates = $repo->getAll();
        //$this->assertContainsOnlyInstancesOf(Candidate::class, $candidates);
        //$this->assertNotEmpty($candidates);
    }

    public function testDelete()
    {
        /***@var $repo ARCandidateRepository***/
        $repo = new ARCandidateRepository();

        /***@var $candidate Candidate***/
        $candidate = $repo->getBy([]);

        //$repo->remove($candidate);
    }
}
