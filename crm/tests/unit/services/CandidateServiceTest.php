<?php
namespace app\crm\tests;

use Codeception\Test\Unit;
use app\crm\dispatchers\SomeEventDispatcher;
use app\crm\entities\candidate\CandidateId;
use app\crm\entities\Contact;
use app\crm\services\candidate\CandidateService;
use app\crm\repositories\candidates\ARCandidateRepository;
use app\crm\services\ContactService;
use app\crm\services\SkillService;
use app\crm\repositories\lib\SkillRepository;
use app\crm\services\dto\CandidateCreateDTO;

class CandidateServiceTest extends Unit
{
    public function testCreateService()
    {
        /***@var $service CandidateService ***/
        $service = new CandidateService(
            new ARCandidateRepository(), 
            new SomeEventDispatcher(), 
            new ContactService(), 
            new SkillService(new SkillRepository()));

        $this->assertInstanceOf(CandidateService::class, $service);
        //codecept_debug($service);
    }

    public function testCreateCandidate()
    {
        $service = new CandidateService(
                    new ARCandidateRepository(), 
                    new SomeEventDispatcher(), 
                    new ContactService(), 
                    new SkillService(new SkillRepository()));

        $candidateDTO = new CandidateCreateDTO();        
        $candidateDTO->load($this->getPOST());

        $service->create($candidateDTO);
    }

    public function testRename()
    {

    }

    public function testRemove()
    {

    }

    public function getPOST(){
        return [
            'name' => 'практикант',
            'lastname' =>  'практик' ,
            'fathername' =>  'практикантович' ,
            'location' =>  '2' ,
            'salary' =>  '9999' ,
            'current_company' =>  'ООО Практик' ,
            'current_position' =>  'практикант',
            'wish' =>  'практиковать практику',
            'birth' =>  '2019-05-24' ,
            'contacts' => [
                '0501415599',
                'practic@gmail.com',
                'practic1999',
                'linkedin.com/practic' 
            ],
            'skills' => ['1','2','24'],
            'comment' =>  'comment' 
        ];
    }


}