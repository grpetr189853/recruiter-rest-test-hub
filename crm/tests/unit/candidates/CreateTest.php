<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 13:39
 */

namespace app\crm\tests\unit\candidates;

use Codeception\Test\Unit;
use app\crm\entities\candidate\Candidate;
use app\crm\entities\candidate\CandidateId;
use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\entities\candidate\SkillCandidate;
use app\crm\entities\candidate\YiiCandidate;
use app\crm\repositories\candidates\ARCandidateRepository;
use PHPUnit\Framework\TestResult;
use yii\helpers\Json;
use app\crm\entities\candidate\ContactCandidate;
use Ramsey\Uuid\Uuid;
use yii\db\Exception as DBException;
use yii\db\IntegrityException;
use app\crm\services\dto\CandidateCreateDTO;


class CreateTest extends Unit
{



    public function testCreate()
    {
        $candidate = CandidateBuilder::instance()->build();
        $candidate->save();

        $this->assertEquals($candidate->getCreateDate()->format('Y-m-d'), date('Y-m-d'));
        $this->assertEquals($candidate->getName()->getLast(),  'Dmytro');
        $this->assertEquals($candidate->getName()->getFirst(), 'Mytsko');
        $this->assertEquals($candidate->getName()->getFather(),'Ivanovich');
        $this->assertEquals($candidate->getName()->getFull(),  'Dmytro Mytsko Ivanovich');

        codecept_debug($candidate->getAttributes());
        
        codecept_debug('ID:       '.$candidate->getId()->getId());
        codecept_debug('NAME:     '.$candidate->getName()->getFull());
        codecept_debug('CREATED:  '.$candidate->getCreateDate()->format('Y-m-d'));
        codecept_debug('BIRTH:    '.$candidate->getBirthDate()->format('Y-m-d'));
        codecept_debug('COMPANY:  '.$candidate->getCompany());
        codecept_debug('POSITION: '.$candidate->getPosition());
        codecept_debug('SALARY:   '.$candidate->getSalary());
        codecept_debug('STATUS:   ');
        codecept_debug('PHONE:    '.$candidate->getContacts()[0]->getValue());
        codecept_debug('EMAIL:    '.$candidate->getContacts()[1]->getValue());
        codecept_debug('SKILL-1:  '.$candidate->getSkills()[0]->skill->name);
    }

    public function testGet()
    {
        /***@var $repo ARCandidateRepository***/
        //$repo = new ARCandidateRepository();
        $candidate = Candidate::find()->andWhere([])->one();
        codecept_debug('LOCATION: '.$candidate->city->name);
    }

    public function testDTO()
    {
        $contacts = ['0501415599','practic@gmail.com','practic1999','linkedin.com/practic'];
        $dto = new CandidateCreateDTO();
        $dto->contacts = $contacts;
        $this->assertEquals($dto->getContacts()[0]->getValue(),'0501415599');
        $this->assertEquals($dto->getContacts()[1]->getValue(),'practic@gmail.com');
        $this->assertEquals($dto->getContacts()[2]->getValue(),'practic1999');
        $this->assertEquals($dto->getContacts()[3]->getValue(),'linkedin.com/practic');
        
    }



}