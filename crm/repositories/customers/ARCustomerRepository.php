<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 29.03.2019
 * Time: 10:42
 */

namespace app\crm\repositories\customers;

use app\crm\entities\candidate\Candidate;
use app\crm\entities\customer\Customer;
use app\crm\entities\customer\CustomerId;
use app\crm\interfaces\ICustomerRepository;
use app\crm\interfaces\IEntityId;
use app\crm\repositories\NotFoundException;
use Ramsey\Uuid\Uuid;
use yii\db\Exception;

class ARCustomerRepository implements ICustomerRepository
{

    /***
     * @return IEntityId
     * @throws \Exception
     */
    public function nextId(): IEntityId
    {
        return new CustomerId(Uuid::uuid4()->toString());
    }

    /**
     * @param $id
     * @return Customer
     */
    public function get($id): Customer
    {
        return $this->getBy(['id'=>$id]);
    }

    /***
     * @param Candidate $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save(Customer $entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }

    /***
     * @param Candidate $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function remove(Customer $entity): void
    {
        if (!$entity->delete()) {
            throw new \RuntimeException('Removing error');
        }
    }

    /**
     * @param array $condition
     * @return Customer
     */
    public function getBy(array $condition): Customer
    {
        if(!$customer = Customer::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('Candidate not found');
        }

        return $customer;
    }

    /***
     * @param Candidate $entity
     * @throws \Exception
     * @throws \Throwable
     */
    public function add(Customer $entity): void
    {
        try {
            $entity->insert();
        }
        catch (Exception $e) {
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }

        /*if (!$entity->insert()) {
            throw new \RuntimeException('saving error');
        }*/
    }

    public function getAll($condition=[]): array
    {

        if(!$customers = Customer::find()->andWhere($condition)->orderBy(['created_at' => SORT_DESC])->all()) {
            throw new NotFoundException('Candidates not found');
        }

        return $customers;
    }

    public function getAllJson()
    {
        $condition=[];
        if(!$customers = Customer::find()->andWhere($condition)->with(['city','relatedContacts'])->asArray()->all()) {
            throw new NotFoundException('Candidates not found');
        }

        return $customers;
    }
}