<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 10:42
 */
namespace app\crm\repositories\users;

use app\crm\entities\user\User;
use app\crm\entities\user\UserId;
use app\crm\interfaces\IUserRepository;
use app\crm\interfaces\IEntityId;
use app\crm\repositories\NotFoundException;
use Ramsey\Uuid\Uuid;
use yii\db\Exception;

class ARUserRepository implements IUserRepository
{

    /***
     * @return IEntityId
     * @throws \Exception
     */
    public function nextId(): IEntityId
    {
        return new UserId(Uuid::uuid4()->toString());
    }

    /**
     * @param $id
     * @return User
     */
    public function get($id): User
    {
        return $this->getBy(['id'=>$id]);
    }

    /***
     * @param User $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function save(User $entity): void
    {
        if ($entity->update() === false) {
            throw new \RuntimeException('Saving error');
        }
    }

    /***
     * @param User $entity
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function remove(User $entity): void
    {
        if (!$entity->delete()) {
            throw new \RuntimeException('Removing error');
        }
    }

    /**
     * @param array $condition
     * @return User
     */
    public function getBy(array $condition): User
    {
        if(!$user = User::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('User not found');
        }

        return $user;
    }

    /***
     * @param User $entity
     * @throws \Exception
     * @throws \Throwable
     */
    public function add(User $entity): void
    {
        try {
            $entity->insert();
        }
        catch (Exception $e) {
            throw new \RuntimeException('Inserting error. '. $e->getMessage());
        }
    }

    public function getAll($condition=[], $search=''): array
    {

        if(!$users = User::find()
            ->andWhere($condition)
            ->andFilterWhere([
                'or',
                ['like', 'name', $search],
                ['like', 'lastname', $search],
            ])
            ->orderBy(['created_at' => SORT_DESC])->all()) {
            throw new NotFoundException('Users not found');
        }

        return $users;
    }

    public function getAllJson()
    {
        $condition = [];
        if (!$users = User::find()->andWhere($condition)->with(['city', 'group'])->asArray()->all()) {
            throw new NotFoundException('Users not found');
        }

        return $users;
    }
}