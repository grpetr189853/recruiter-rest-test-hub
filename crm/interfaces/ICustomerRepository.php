<?php


namespace app\crm\interfaces;


use app\crm\entities\customer\Customer;

interface ICustomerRepository
{
    public function get($id): Customer;
    public function add(Customer $entity):void;
    public function save(Customer $entity): void;
    public function remove(Customer $entity): void;
    public function getBy(array $condition): Customer;
    public function nextId(): IEntityId;
    public function getAll(): array;
}