<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 14:29
 */

namespace app\crm\interfaces;


use app\crm\entities\City;

interface ICityRepository
{
    public function get($id): City;
    public function getBy(array $condition);
}