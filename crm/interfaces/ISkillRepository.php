<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 16.04.2019
 * Time: 15:40
 */

namespace app\crm\interfaces;


use app\crm\entities\Skill;

interface ISkillRepository
{
    public function get($id): Skill;
    public function getBy(array $condition);
}