<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 10:37
 */

namespace app\crm\interfaces;


interface IEntity
{
    public function releaseEvents(): array;
    public function export(): array;
    public function getType(): string;
}