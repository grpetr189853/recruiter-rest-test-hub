<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 12:04
 */

namespace app\crm\interfaces;


interface IContactService
{
    public function addContact(IEntityId $entity_id, IContact $contact):void;
    public function removeContact(IEntityId $entity_id, IContact $contact):void;
    public function changeContact(IEntityId $entity_id, IContact $contact):void;
}