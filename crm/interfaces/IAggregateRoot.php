<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 10:28
 */

namespace app\crm\interfaces;


interface IAggregateRoot
{
    public function releaseEvents(): array;
}