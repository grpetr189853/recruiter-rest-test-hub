<?php


namespace app\crm\interfaces;


use app\crm\entities\vacancy\Vacancy;

interface IVacancyRepository
{
    public function get($id): Vacancy;
    public function add(Vacancy $entity): void;
    public function save(Vacancy $entity): void;
    public function remove(Vacancy $entity): void;
    public function getBy(array $condition): Vacancy;
    public function nextId(): IEntityId;
    public function getAll(): array;
}