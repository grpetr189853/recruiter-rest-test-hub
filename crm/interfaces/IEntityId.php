<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 12:36
 */

namespace app\crm\interfaces;


interface IEntityId
{
    public function getId();
}