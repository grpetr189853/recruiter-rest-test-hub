<?php

namespace app\crm\interfaces;


interface ICompany
{
    public function getName();
}