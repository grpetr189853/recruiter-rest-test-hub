<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 10:18
 */

namespace app\crm\interfaces;


interface IRepository
{
    public function get($id);
    public function add( $entity):void;
    public function save( $entity): void;
    public function remove( $entity): void;
    public function getBy(array $condition);
    public function getAll(array $condition);
    public function nextId();
}