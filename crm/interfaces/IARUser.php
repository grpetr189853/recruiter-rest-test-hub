<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 15:54
 */
namespace app\crm\interfaces;


interface IARUser
{
    public function insert();
    public function update();
    public function delete();

}