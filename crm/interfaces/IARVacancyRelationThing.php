<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 04.04.2019
 * Time: 10:57
 */

namespace app\crm\interfaces;


interface IARVacancyRelationThing
{
    public static function tableName(): string;
    public function afterFind(): void;
    public function beforeSave($insert): bool;
}