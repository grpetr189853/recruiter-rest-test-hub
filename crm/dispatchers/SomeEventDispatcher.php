<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 12:32
 */

namespace app\crm\dispatchers;


use app\crm\interfaces\IEventDispatcher;

class SomeEventDispatcher implements IEventDispatcher
{
    public function dispatch($event): void
    {
        \Yii::info('Dispatch event ' . \get_class($event));
    }

    public function dispatchAll(array $events): void
    {
        foreach ($events as $event) {
            \Yii::info('Dispatch event ' . \get_class($event));
        }
    }
}