<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 13:50
 */

namespace app\crm\services;


use app\crm\entities\candidate\ContactCandidate;
use app\crm\entities\candidate\Contacts;
use app\crm\interfaces\IContact;
use app\crm\interfaces\IContactService;
use app\crm\interfaces\IEntityId;

class ContactService implements IContactService
{

    /**
     * @param IEntityId $entity_id
     * @param IContact $contact
     */
    public function addContact(IEntityId $entity_id, IContact $contact): void
    {
        // TODO: Implement addContact() method.
    }

    /**
     * @param IEntityId $entity_id
     * @param IContact $contact
     */
    public function removeContact(IEntityId $entity_id, IContact $contact): void
    {
        // TODO: Implement removeContact() method.
    }

    /**
     * @param IEntityId $entity_id
     * @param IContact $contact
     */
    public function changeContact(IEntityId $entity_id, IContact $contact): void
    {
        echo '5';
    }

    public function setContacts(Contacts $curr_contacts, array $formContacts)
    {
        $new_contacts = [];
        if(!empty($formContacts)) {
            /**создадим  из массива*/
            foreach ($formContacts as $key => $formContact) {
                if (!empty($formContact[key($formContact)])) {
                    $new_contacts[] = new ContactCandidate(key($formContact), $formContact[key($formContact)]);
                }
            }
        }
        return new Contacts($new_contacts);
    }

    public function getTypes()
    {
        //TODO:
    }
}