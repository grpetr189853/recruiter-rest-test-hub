<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 11:13
 */
namespace app\crm\services\user;

use app\crm\entities\candidate\Info;
use app\crm\entities\user\User;
use app\crm\entities\candidate\Name;
use app\crm\forms\user\UserEditForm;
use app\crm\interfaces\IUserRepository;
use app\crm\interfaces\IUserService;
use app\crm\interfaces\IEntityId;
use app\crm\interfaces\IEventDispatcher;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\UserCreateDTO;
use app\crm\services\dto\NameDTO;

class UserService implements IUserService
{
    /***@var $repo IUserRepository***/
    private $users;

    /***@var $dispatcher IEventDispatcher***/
    private $dispatcher;


    public function __construct(IUserRepository $repo, IEventDispatcher $dispatcher)
    {
        $this->users = $repo;
        $this->dispatcher = $dispatcher;
    }

    public function create(UserCreateDTO $dto)
    {
        $user = User::create(
            $dto->getName(),
            $dto->date_birth,
            $dto->location_id,
            $dto->email,
            $dto->group,
            $dto->password,
            $dto->repeat_password,
            $dto->status,
            $dto->getInfo()
        );
        $this->users->add($user);

        return $user->getId()->getId();
    }

    public function edit($id, UserEditForm $form)
    {
        $user = $this->users->get($id);

        $user->rename(new Name($form->lastname, $form->name, null));

        $user->setInfo(new Info(null, null, null, new \DateTimeImmutable($form->date_birth)));

        $user->setLocation($form->location_id);

        $user->setEmail($form->email);

        $user->setStatus($form->status);
        
        if(!empty($form->old_password)) {
            $user->setPassword($form->password);
        }
        
        try {
            $this->users->save($user);
        }
        catch (\yii\db\IntegrityException  $ie) {
            die($ie->getMessage());
        }
        catch (\DomainException $de) {
            die($de->getMessage());
        }
    }

    public function rename(IEntityId $id, NameDTO $name): void
    {
        $user = $this->users->get($id->getId());
        $name = new Name($name->first, $name->last);
        $user->rename($name);
        try {
            $this->users->save($user);
        }

        catch (\yii\db\Exception $exception) {die;}
        catch (\DomainException $exception) {die;}

        catch (\yii\base\Exception $exception){die;}

        catch (\Exception $exception){die;}
    }

    public function remove($id): void
    {
        $user = $this->users->get($id);
        $user->remove();
        $this->users->remove($user);
    }

    private function toName(NameDTO $name)
    {
        $name = new Name(
            $name->last,
            $name->first
        );

        return $name;
    }

    public function getAll($condition=[], $search=''): array
    {
        try{
            $users = $this->users->getAll($condition, $search);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $users = [];
        }

        return $users;
    }

    public function getAllJson(): array
    {
        $users = $this->users->getAllJson();
        return $users;
    }

    public function getBy(array $condition)
    {
        return $this->users->getBy($condition);
    }
}