<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 28.03.2019
 * Time: 11:13
 */

namespace app\crm\services\vacancy;

use app\crm\entities\vacancy\Vacancy;
use app\crm\interfaces\IVacancyRepository;
use app\crm\interfaces\IVacancyService;
use app\crm\interfaces\IEntityId;
use app\crm\interfaces\IEventDispatcher;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\VacancyCreateDTO;

class VacancyService implements IVacancyService
{
    /***@var $repo IVacancyRepository***/
    private $vacancies;

    /***@var $dispatcher IEventDispatcher***/
    private $dispatcher;

    public function __construct(IVacancyRepository $repo, IEventDispatcher $dispatcher)
    {
        $this->vacancies   = $repo;
        $this->dispatcher   = $dispatcher;
    }

    public function create(VacancyCreateDTO $dto)
    {
        $id = $this->vacancies->nextId();
        $dto->id = $id->getId();
        $vacancy = Vacancy::create(
            $id,
            $dto->getName(),
            $dto->location,
            $dto->salary,
            $dto->customers,
            $dto->company,
            $dto->getSkills(),
            $dto->v_duties,
            $dto->w_conditions,
            $dto->v_requirements,
            $dto->v_description,
            $dto->getDocuments()
        );
        $this->vacancies->add($vacancy);

        return $id->getId();
        //$this->dispatcher->dispatch($vacancy->releaseEvents());
    }

    public function rename(IEntityId $id, string $name): void
    {
        $vacancy = $this->vacancies->get($id->getId());
        $vacancy->rename($name);
        try {
            $this->vacancy->save($vacancy);
        }
            // db related exceptions
        catch (\yii\db\Exception $exception) {die;}
        catch (\DomainException $exception) {die;}

        // any exception throwin by yii
        catch (\yii\base\Exception $exception){die;}

        // any php exception
        catch (\Exception $exception){die;}
    }

    public function remove(IEntityId $id): void
    {
        $vacancy = $this->vacancies->get($id->getId());
        $vacancy->remove();
        $this->vacancies->remove($vacancy);
        $this->dispatcher->dispatch($vacancy->releaseEvents());
    }

    private function toName(string $name)
    {
        return $name;
    }

    public function getAll($condition=[]): array
    {
        try{
            $vacancies = $this->vacancies->getAll($condition);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $vacancies = [];
        }

        return $vacancies;
    }

    public function getAllJson(): array
    {
        $vacancies = $this->vacancies->getAllJson();
        return $vacancies;
    }

    public function getBy(array $condition)
    {
        return $this->vacancies->getBy($condition);
    }
}