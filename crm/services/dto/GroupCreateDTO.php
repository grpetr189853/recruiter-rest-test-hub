<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 12:20
 */
namespace app\crm\services\dto;

class GroupCreateDTO
{
    public $name;

    public function load(array $params)
    {
        $this->name = ($params['name']) ?? '';
    }
}