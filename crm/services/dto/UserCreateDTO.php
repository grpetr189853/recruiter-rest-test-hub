<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 12:20
 */
namespace app\crm\services\dto;

use common\forms\user\UserCreate;
use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\entities\user\User;

class UserCreateDTO
{
    public $name;
    public $lastname;
    public $date_birth;
    public $location_id;
    public $email;
    public $group;
    public $password;
    public $repeat_password;
    public $status;

    public function load(array $params)
    {
        $this->name = ($params['name']) ?? '';
        $this->lastname = ($params['lastname']) ??'';
        $this->date_birth   = ($params['date_birth']) ?? '';
        $this->location_id  = (is_numeric($params['location_id'])) ? $params['location_id'] : null;
        $this->email     = ($params['email']) ??'';
        $this->group    = (($params['group'])) ? $params['group'] : null;
        $this->password = ($params['password']) ?? '';
        $this->repeat_password = ($params['repeat_password']) ?? '';
        $this->status = ($params['status']) ?? User::STATUS_ACTIVE;
    }

    /***
     * @return Name
     */
    public function getName(): Name
    {
        return new Name(
            $this->lastname,
            $this->name
        );
    }

    /***
     * @return Info
     */
    public function getInfo(): Info
    {
        return new Info(
            null,
            null,
            null,
            new \DateTimeImmutable($this->date_birth)
        );
    }

}