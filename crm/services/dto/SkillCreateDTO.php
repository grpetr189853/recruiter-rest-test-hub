<?php


namespace app\crm\services\dto;


class SkillCreateDTO
{
    public $name;

    public function load(array $params)
    {
        $this->name = ($params['name']) ?? '';
    }
}