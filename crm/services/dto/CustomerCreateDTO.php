<?php
/**
 * Created by PhpStorm.
 * User: Dmytro
 * Date: 29.03.2019
 * Time: 12:20
 */

namespace app\crm\services\dto;

use app\crm\entities\candidate\Name;
use app\crm\entities\customer\ContactCustomer;
use app\crm\entities\ContactAbstract as CA;

class CustomerCreateDTO
{
    public $name;
    public $lastname;
    public $fathername;
    public $location;
    public $contacts = [];

    public function load(array $params)
    {
        $this->name         = ($params['name']) ?? '';
        $this->lastname     = ($params['lastname']) ??'';
        $this->fathername   = ($params['fathername']) ??'';
        $this->location     = (is_numeric($params['location'])) ? $params['location'] : null;
        $this->contacts     = ($params['contacts']) ?? [];
    }

    /***
     * @return Name
     */
    public function getName(): Name
    {
        return new Name(
            $this->lastname,
            $this->name,
            $this->fathername
        );
    }

    /***
     * Convert array into Customer contacts entity array
     *
     * @return array
     */
    public function getContacts(): array
    {
        $contacts=[];

        foreach ($this->contacts as $contact){
            if( preg_match('/^\+?\d+$/', $contact) ){
                $contacts[] = new ContactCustomer(CA::PHONE, $contact);
            }
            elseif(preg_match('/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/', $contact) ){
                $contacts[] = new ContactCustomer(CA::EMAIL, $contact);
            }
            elseif( preg_match('/\blinkedin\b/', $contact) ) {
                $contacts[] = new ContactCustomer(CA::LINKEDIN, $contact);
            }
            elseif( preg_match('/^[a-z][a-z0-9\.,\-_]{5,31}$/i', $contact) ) {
                $contacts[] = new ContactCustomer(CA::SKYPE, $contact);
            }
        }

        return $contacts;
    }
}