<?php


namespace app\crm\services\dto;


class CountryCreateDTO
{
    public $name;

    public function load(array $params)
    {
        $this->name = ($params['name']) ?? '';
    }
}