<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 29.03.2019
 * Time: 12:20
 */

namespace app\crm\services\dto;


use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\entities\candidate\ContactCandidate;
use app\crm\entities\candidate\SkillCandidate;
use app\crm\entities\candidate\DocumentCandidate;
use app\crm\entities\ContactAbstract as CA;

class CandidateCreateDTO
{
    public $id;
    public $name;
    public $lastname;
    public $fathername;
    public $salary;
    public $company;
    public $position;
    public $contacts= [];
    public $skills  = [];
    public $birth;
    public $comment;
    public $wish;
    public $location;
    public $photo;
    public $documents = [];

    public function load(array $params)
    {
        $this->id           = ($params['id']) ?? '';
        $this->name         = ($params['name']) ?? '';
        $this->lastname     = ($params['lastname']) ??'';
        $this->fathername   = ($params['fathername']) ??'';
        $this->location     = (is_numeric($params['location'])) ? $params['location'] : null ;
        $this->salary       = (is_numeric($params['salary'])) ? $params['salary'] : 0;
        $this->company      = ($params['current_company']) ?? null;
        $this->position     = ($params['current_position']) ?? null;
        $this->wish         = ($params['wish']) ?? '';
        $this->birth        = ($params['birth']) ?? '';
        $this->contacts     = ($params['contacts']) ?? [];
        $this->skills       = (is_array($params['skills'])) ? $params['skills'] : [];
        $this->comment      = ($params['comment']) ?? '';
        $this->photo        = ($params['photo']) ?? '';
        $this->comment      = ($params['comment'] ?? '');
        $this->documents    = (is_array($params['documents'])) ? $params['documents'] : [];
    }

    /***
     * @return Name
     */
    public function getName(): Name
    {
        return new Name(
            $this->lastname,
            $this->name,
            $this->fathername
        );
    }

    /***
     * @return Info
     */
    public function getInfo(): Info
    {
        return new Info(
            $this->salary,
            $this->company,
            $this->position,
            new \DateTimeImmutable($this->birth)
        );
    }


    /***
     * Convert array into Candidate contacts entity array
     *
     * @return array
     */
    public function getContacts(): array
    {
        $contacts=[];

        foreach ($this->contacts as $contact){
            if( preg_match('/^\+?\d+$/', $contact) ){
                $contacts[] = new ContactCandidate(CA::PHONE, $contact);
            }
            elseif(preg_match('/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/', $contact) ){
                $contacts[] = new ContactCandidate(CA::EMAIL, $contact);
            }
            elseif( preg_match('/\blinkedin\b/', $contact) ) {
                $contacts[] = new ContactCandidate(CA::LINKEDIN, $contact);
            }
            elseif( preg_match('/^[a-z][a-z0-9\.,\-_]{5,31}$/i', $contact) ) {
                $contacts[] = new ContactCandidate(CA::SKYPE, $contact);
            }
        }

        return $contacts;
    }

    public function getSkills(): array
    {
        $skills=[];
        foreach ($this->skills as $item) {
            $skills[] = new SkillCandidate($item);
        }

        return $skills;
    }

    public function getDocuments(): array
    {
        $documents = [];
        $filteredArrayOfDocuments = array_filter($this->documents);
        if(!empty($filteredArrayOfDocuments)){
            foreach ($this->documents as $document){
                $documents[] = new DocumentCandidate(basename($document),'pdf',$this->id);
            }
        }

        return $documents;
    }
}