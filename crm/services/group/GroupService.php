<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 11:13
 */
namespace app\crm\services\group;

use app\crm\entities\group\Group;
use app\crm\interfaces\IGroupRepository;
use app\crm\interfaces\IGroupService;
use app\crm\interfaces\IEntityId;
use app\crm\interfaces\IEventDispatcher;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\GroupCreateDTO;

class GroupService implements IGroupService
{
    /***@var $repo IGroupRepository***/
    private $groups;

    /***@var $dispatcher IEventDispatcher***/
    private $dispatcher;


    public function __construct(IGroupRepository $repo, IEventDispatcher $dispatcher)
    {
        $this->groups = $repo;
        $this->dispatcher = $dispatcher;
    }

    public function create(GroupCreateDTO $dto)
    {
        $group = Group::create(
            $dto->name
        );
        
        $this->groups->add($group);

        return $group->getName();
    }

    public function getAll($condition=[], $search=''): array
    {
        try{
            $groups = $this->groups->getAll($condition, $search);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $groups = [];
        }

        return $groups;
    }

    public function getAllJson(): array
    {
        $groups = $this->groups->getAllJson();
        return $groups;
    }

    public function getBy(array $condition)
    {
        return $this->groups->getBy($condition);
    }
}