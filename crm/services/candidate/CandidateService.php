<?php
/**
 * Created by PhpStorm.
 * User: Rox
 * Date: 28.03.2019
 * Time: 11:13
 */

namespace app\crm\services\candidate;


use app\crm\entities\candidate\Candidate;
use app\crm\entities\candidate\Info;
use app\crm\entities\candidate\Name;
use app\crm\forms\candidate\ProfileEditForm;
use app\crm\interfaces\ICandidateRepository;
use app\crm\interfaces\ICandidateService;
use app\crm\interfaces\IContact;
use app\crm\interfaces\IContactService;
use app\crm\interfaces\IDocumentService;
use app\crm\interfaces\IEntity;
use app\crm\interfaces\IEntityId;
use app\crm\interfaces\IEventDispatcher;
use app\crm\interfaces\ISkillService;
use app\crm\repositories\NotFoundException;
use app\crm\services\dto\CandidateCreateDTO;
use app\crm\services\dto\NameDTO;
use phpDocumentor\Reflection\Types\Integer;
use yii\db\Exception;
use yii\db\IntegrityException;

class CandidateService implements ICandidateService, IContactService
{
    /***@var $repo ICandidateRepository***/
    private $candidates;

    /***@var $dispatcher IEventDispatcher***/
    private $dispatcher;

    /***@var $contact IContactService****/
    private $contact;

    /***@var $contact ISkillService****/
    private $skill;
    /***@var $document IDocumentService***/
    private $document;

    public function __construct(ICandidateRepository $repo, IEventDispatcher $dispatcher, IContactService $contact, ISkillService $skill, IDocumentService $document)
    {
        $this->candidates   = $repo;
        $this->dispatcher   = $dispatcher;
        $this->contact      = $contact;
        $this->skill        = $skill;
        $this->document     = $document;
    }

    public function create(CandidateCreateDTO $dto)
    {
        $id = $this->candidates->nextId();
        $dto->id = $id->getId();
        $candidate = Candidate::create(
            $id,
            $dto->getName(),
            $dto->getInfo(),
            $dto->photo,
            $dto->comment,
            $dto->wish,
            $dto->location,
            $dto->getContacts(),
            $dto->getSkills(),
            $dto->getDocuments()
        );
        $this->candidates->add($candidate);

        return $id->getId();
        //$this->dispatcher->dispatch($candidate->releaseEvents());
    }


    public function edit($id, ProfileEditForm $form)
    {
        $candidate = $this->candidates->get($id);

        $candidate->rename(new Name($form->lastname, $form->name, $form->fathername));

        $candidate->setInfo(new Info($form->salary, $form->current_company, $form->current_position, new \DateTimeImmutable($form->birth)));

        $candidate->setLocation($form->location);

        $candidate->setCurrentSkills($this->setSkills($form->skills, $candidate));

        $candidate->setCurrentContacts($this->setContacts($form->contacts, $candidate->getCurrentContacts()));

        /*
        foreach ($form->documents as $document){

            $candidate->addDocument($document);

        }
        */
        $candidate->setCurrentDocuments($this->setDocuments($form->documents, $candidate));

        $candidate->setPhoto($form->photo);

        $candidate->setNotes($form->notes);

        try {
            $this->candidates->save($candidate);
        }
        catch (\yii\db\IntegrityException  $ie) {
            die($ie->getMessage());
        }
        catch (\DomainException $de) {
            die($de->getMessage());
        }

    }

    public function rename(IEntityId $id, NameDTO $name): void
    {
        $candidate = $this->candidates->get($id->getId());
        $name = new Name($name->first, $name->last, $name->father);
        $candidate->rename($name);
        try {
            $this->candidates->save($candidate);
        }
            // db related exceptions
        catch (\yii\db\Exception $exception) {die;}
        catch (\DomainException $exception) {die;}

        // any exception throwin by yii
        catch (\yii\base\Exception $exception){die;}

        // any php exception
        catch (\Exception $exception){die;}
    }

    public function remove(IEntityId $id): void
    {
        $candidate = $this->candidates->get($id->getId());
        $candidate->remove();
        $this->candidates->remove($candidate);
        $this->dispatcher->dispatch($candidate->releaseEvents());
    }

    public function addContact(IEntityId $entity_id, IContact $contact): void
    {
        $candidate = $this->candidates->get($entity_id);
        $this->contact->addContact($contact);
        $this->candidates->save($candidate);
        $this->dispatcher->dispatch($candidate->releaseEvents());

    }

    public function changeContact(IEntityId $entity_id, IContact $contact): void
    {
        $this->contact->changeContact($entity_id, $contact);
    }

    public function removeContact(IEntityId $entity_id, IContact $contact): void
    {
        // TODO: Implement removeContact() method.
    }

    private function toName(NameDTO $name)
    {
        $name = new Name(
            $name->last,
            $name->first,
            $name->father
        );

        return $name;
    }

    public function getAll($condition=[], $search=''): array
    {
        try{
            $candidates =$this->candidates->getAll($condition, $search);
        }
        catch (NotFoundException $e) {
            //TODO: log
            $candidates = [];
        }

        return $candidates;
    }

    public function getAllJson(): array
    {
        $candidates =$this->candidates->getAllJson();
        return $candidates;
    }

    public function getBy(array $condition)
    {
        return $this->candidates->getBy($condition);
    }

    private function setSkills($formSkills, $candidate)
    {
        $form_skills    = (is_array($formSkills)) ? $formSkills : [];
        $new_skills     = $this->skill->setSkills($candidate->getCurrentSkills(), $form_skills);

        return $new_skills;
    }

    private function setContacts($formContacts, $currentContacts)
    {
        $new_contacts   = $this->contact->setContacts($currentContacts, $formContacts);
        return $new_contacts;
    }

    private function setDocuments($formDocuments, $candidate)
    {
        $form_documents = (is_array($formDocuments)) ? $formDocuments : [];
        $new_documents  = $this->document->setDocuments($candidate->getCurrentDocuments(),$form_documents);

        return $new_documents;
    }

}