<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\common\bootstrap\Setup'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KkDF5NnbNMGCSH96cwn3ROrgil-6Z36N',
            'enableCsrfValidation' => false,
            'csrfParam' => '_csrf-frontend',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'authManager'  => [
            'class'        => 'yii\rbac\DbManager',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/about',
//                'candidates' => 'site/about',
                ['class' => 'yii\rest\UrlRule', 'controller' => 'candidate',
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>'
                    ],
                    'extraPatterns' => [
                        'POST {id}' => 'create',
                        'POST {id}/photo' => 'upload-photo',
                        'DELETE {id}/photo' => 'delete-photo',
                    ],
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'company',
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>'
                    ],
                ],                
                ['class' => 'yii\rest\UrlRule', 'controller' => 'customer',
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>'
                    ],
                    'extraPatterns' => [
                    ],
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'vacancy',
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>'
                    ],
                    'extraPatterns' => [
                    ],
                ],                
                ['class' => 'yii\rest\UrlRule', 'controller' => 'cities',
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ]
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'skills',
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ]
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'contacts-type',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ]
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'skills-candidate',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ],
                    'prefix' => 'candidates/<candidate_id:\\w+-\\w+-\\w+-\\w+-\\w+>',
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'skills-vacancy',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ],
                    'prefix' => 'vacancies/<vacancy_id:\\w+-\\w+-\\w+-\\w+-\\w+>',
                ],                
                ['class' => 'yii\rest\UrlRule', 'controller' => 'contacts-candidate',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ],
                    'prefix' => 'candidates/<candidate_id:\\w+-\\w+-\\w+-\\w+-\\w+>',
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'documents-candidate',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ],
                    'prefix' => 'candidates/<candidate_id:\\w+-\\w+-\\w+-\\w+-\\w+>',
                    'extraPatterns' => [
                        'DELETE' => 'delete'
                    ],
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'documents-vacancy',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ],
                    'prefix' => 'vacancies/<vacancy_id:\\w+-\\w+-\\w+-\\w+-\\w+>',
                    'extraPatterns' => [
                        'DELETE' => 'delete'
                    ],
                ],                
                ['class' => 'yii\rest\UrlRule', 'controller' => 'statuses-candidate',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ],
                    'prefix' => 'candidates/<candidate_id:\\w+-\\w+-\\w+-\\w+-\\w+>',
                    'extraPatterns' => [
                        'DELETE' => 'delete'
                    ],
                ],                  
                ['class' => 'yii\rest\UrlRule', 'controller' => 'contacts-customer',
                    'pluralize' => false,
                    'tokens' => [
                        '{id}' => '<id:\\d+>',
                    ],
                    'prefix' => 'customers/<customer_id:\\w+-\\w+-\\w+-\\w+-\\w+>',
                ],
                ['class' => 'yii\rest\UrlRule', 'controller' => 'mail',
                    'pluralize' => false,
                ],                
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap4\BootstrapAsset' => [
                    'sourcePath' => null,
                    'baseUrl' => 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1',
                    'css' => [
                        'css/bootstrap.min.css'
                    ],
                ],
                'yii\bootstrap4\BootstrapPluginAsset' => [
                    'sourcePath' => null,
                    'baseUrl' => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1',
                    'js' => [
                        'js/bootstrap.bundle.min.js'
                    ],
                ],
                'yii\bootstrap\BootstrapAsset'=>[
                    'css'=>[],
                ],
                'yii\bootstrap\BootstrapPluginAsset'=>[
                    'js'=>[],
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
